###### Join our telegram group: https://t.me/GeneronFramework

---

# Generon: Low-Level Efficiency with a High-Level Interface.

## Project overview

`Playground` is made as a `demo` project for the `Generon Framework`. It has examples and documentation which explains
features and theory behind it. Each example has own directory and `.md` file. Classes from example directories does
not interact with each other and are independent which makes it easier to understand. Despite the fact that examples are
logically separated by different directories, the code generator will see everything as single project.

There is `additional_theory` directory which contains some explanations which are not directly related to any example.

## What is Generon ?

Generon is a code generation framework built on top of Aeron Cluster. It offers efficiency and power of low-level
distributed systems, makes development easier by removing complexity and providing High-Level interface without
performance impact and allows to build fast, flexible, secure and easily maintainable backend system.

## Why Generon ?

1. Speed: Using event-sourcing Aeron base and code generation design as abstraction layer, cluster can process more than
   a million commands per second while keeping high level experience.
2. Complete Code Generation: Generon takes away the complexity – generates low level serialization/deserialization
   using own protocol, client SDK, event producers and listeners, dependency injection, multi-tenancy management,
   flyweight management, optimized direct read-only requests and much more. In addition, there is no proxies and hidden
   functionalities.
3. Solve Complex Problems: Snapshots, migrations, communication, reconnections, data and api versioning, rolling
   updates, security, property management, scheduling, load balancing for event consumers and direct read-only
   requests - all is supported out the box, minimizing manual intervention and errors.
4. Optimized Development: Using Generon, development time is almost 100% about writing business logic with simple Java
   Core code. It means there is no need to use third party databases, messaging queue systems and other tools. No need
   to write boilerplate code and deal with serialization/deserialization, transactions, connections, integrations and
   engage in architectural planning. As result a small mid-level team will develop high quality product much faster and
   cheaper due to removed complexity, which usually requires expect level experience and big teams.
5. Rich Feature Set: It provides abstractions and built-in solutions for complex challenges. Generon is annotation based
   framework and has lots of hooks and configurations to ensure that each challenge has elegant and flexible solution.
6. Multi-Dimensional: Generon is a perfect choice for complex products like SaaS, exchanges and matching engines,
   databases, realtime messaging systems and smaller products which require speed, high-availability, data safety and
   distribution.

## How does code generation work ?

Code is generated automatically when build is triggered. When project is cloned for the first time it will show some
missed classes. To resolve that and generate classes we need to make first build - run some examples or do usual
"mvn clean install". After it, we can operate as usual.

## How does it generate code ?

1. Generon has `annotation processor` which scans classes, methods, fields and annotations at compile time.
2. Then it will call remote code generation server to build classes
3. Based on the response it will write source code classes to the `target` directory which will be compiled on the next
   phase.

It will use cache to avoid redundant server calls when annotation configuration is not changed. Cache file is saved
under the target directory.

Note! It uses java annotation processing api, and it does not collect code itself. Generon will skip classes if it does
not use framework annotations.

All what Generon needs for build is:

1. Classes: name, package name, supertypes, generics and annotations
2. Fields: name, type and annotations.
3. Methods: name, parameter names and types, return type and annotations.
4. System properties which can be found under `GeneronProperties` class

To see what is sent to the server we can debug plugin itself, which has open code base, or update `build-config.json` to
have `"saveRequest": true` which will put request json under target directory during build.

## What is considered as entry point for the Generon Framework ?

All begins with `GeneronNode` and `GeneronClient` classes. These classes have `launch` method with context
parameter. `GeneronNode` has `GeneronNodeContext` and `GeneronClient` has `GeneronClientContext`. When `launch` methods
are called all is up and running.

Minimal configuration for `GeneronNodeContext` requires:

1. `int nodeId`. It is a node identifier within the cluster. If there is single node cluster we can set 0, if there is 3
   nodes we can set 0, 1, 2 or any other values. This id is used for nodes to know who is who, do leader election etc.
2. `List<GeneronClusterMember> clusterMembers`. List of `GeneronClusterMember` objects which can be created
   by `GeneronClusterMember.create(nodeId, hostname)`. This class contains `host name` and `ports` for each node.
   Each node should have info about all cluster members including itself. If there is 3 nodes in the cluster each
   node (GeneronNodeContext) `should contain exactly same` List<GeneronClusterMember> with size of 3.
3. `String aeronDirectory`. This directory is used for the Aeron itself to manage communications, data streams etc.
   Aeron uses memory mapped files and this directory `should use devshm` folder like `/dev/shm/aeron` in
   production to get maximum speed. Data from this directory does not need persistence and can be deleted when node
   restarted. There is `GeneronNodeContext.withDevShm(...)` with preconfigured directory.
4. `String componentDirectory`. This directory is used for Aeron components to manage data replication (Raft Log) and
   Archive data streams (queues) which requires persistence. This directory should be on the disk and should not point
   to `/dev/shm` or any other in-memory directory.
5. `GeneronNodeClientContext clientContext`. This object contains client related information required for the node to
   know. Currently, it has direct communication port and stream id which node will use to send responses back to the
   client. We can use `AeronClientConfig.toNodeClientContext()` to build an object based on the client configuration.

`/dev/shm` is a linux directory, macOS and Windows do not support such behavior by default, and it requires additional
steps to take. To make it simple, configuration of example is using same disk folder for both parameters.

Minimal configuration for `GeneronClientContext` requires:

1. `List<GeneronClusterMember> clusterMembers`. Same list that GeneronNodeContext gets. Used for command, direct call
   and event communication setup.
2. `int requestThreadPoolSize`. Amount of parallel write requests which client can make to the cluster at the same time.
3. `int eventConsumerThreadPoolSize`. Amount of threads which will poll events for consumers. Multiple consumers can be
   assigned to the same thread by round-robin.
4. `String aeronDirectory`. Same as from `GeneronNodeContext`, it also has `GeneronClientContext.withDevShm(...)` method
   which can be used.
5. `AeronClientProperties aeronClientProperties`. This class contains properties required for cluster connection. It
   has info like ports, channels and stream ids. This class can be created based on any `GeneronClusterMember` using for
   example `AeronClientConfig.of(clusterMembers.getFirst())`, it will find leader automatically using any node.

Playground has `TestRunner` class which encapsulates this logic. It can run multiple
nodes - `TestRunner.NODE_AMOUNT = N;`, where N - odd number. All can be under same JVM or host, which simplifies
testing, debugging and development. We can have as many nodes as we want, but it depends on port availability and
overlaps. By default, each node have port shift by 20 starting from 10000. If each node has its own IP or DNS name, we
can set the shift to 0 to ensure they all use the same ports.

Run requires VM option: `--add-opens java.base/sun.nio.ch=ALL-UNNAMED`

## How to write first api ?

1. Create `HelloWorld` interface with `@Service(S)` annotation at class level, S - service id.
2. Define some methods there with `@ServiceMethod(M)` annotation, M - method id.
    ```java
    @Service(1)
    public interface HelloWorldService {

        @ServiceMethod(1)
        String hello();

    }
   ```
3. Create `HelloWorldImpl` and implement `HelloWorld` interface.
   ```java
   public class HelloWorldServiceImpl implements HelloWorldService {    
    
        @Override
        public String hello() {
            return "Hello, Generon";
        }

   }
   ```
4. Prepare client and node contexts:
   ```java
   // Node
   int nodeId = 0;
   GeneronClusterMember member = GeneronClusterMember.create(nodeId, "localhost");
   List<GeneronClusterMember> clusterMembers = Collections.singletonList(member);

   GeneronNodeClientContext nodeClientContext = AeronClientConfig.toNodeClientContext();
   GeneronNodeContext generonNodeContext = new GeneronNodeContext(nodeId, clusterMembers, directoryPrefix + "/node" + nodeId, nodeClientContext);

   generonNodeContext.enableComponentCleanup(); // Remove disk-related data to make each run isolated

   // Client
   AeronClientProperties aeronClientProperties = AeronClientConfig.of(clusterMembers.getFirst());
   GeneronClientContext generonClientContext = new GeneronClientContext(clusterMembers, 2, 1, directoryPrefix + "/client", aeronClientProperties);
   ```
5. Do `GeneronNode.launch` and `GeneronClient.launch` to start application.
   ```java
   GeneronNode generonNode = GeneronNode.launch(generonNodeContext);
   GeneronClient generonClient = GeneronClient.launch(generonClientContext);
   ```
6. Create final API class
   ```java
   ClientAPI clientAPI = generonClient.api(ClientAPi::new);
   TenantClientAPI tenantAPI = clientAPI.tenantAPI(1);

   HelloWorldServiceFactory helloWorldServiceFactory = tenantAPI.helloWorldServiceFactory();
   HelloWorldServiceBlockingAPI blockingApi = helloWorldServiceFactory.blocking();
   ```
7. Call API to get result:
   ```java
   System.out.println("Result: " + blockingApi.hello());
   ```
8. Close resources in reverse order:
   ```java
   generonClient.close();
   generonNode.close();
   ```

### Complete example:

   ```java
public class HelloWorld {

    public static void main(String[] args) {
        String directoryPrefix = "aeron_directory";

        // Node
        int nodeId = 0;
        GeneronClusterMember member = GeneronClusterMember.create(nodeId, "localhost");
        List<GeneronClusterMember> clusterMembers = Collections.singletonList(member);

        GeneronNodeClientContext nodeClientContext = AeronClientConfig.toNodeClientContext();
        GeneronNodeContext generonNodeContext = new GeneronNodeContext(nodeId, clusterMembers, directoryPrefix + "/node" + nodeId, nodeClientContext);

        generonNodeContext.enableComponentCleanup(); // Remove disk-related data to make each run isolated

        // Client
        AeronClientProperties aeronClientProperties = AeronClientConfig.of(clusterMembers.getFirst());
        GeneronClientContext generonClientContext = new GeneronClientContext(clusterMembers, 2, 1, directoryPrefix + "/client", aeronClientProperties);

        // Launch
        GeneronNode generonNode = GeneronNode.launch(generonNodeContext);
        GeneronClient generonClient = GeneronClient.launch(generonClientContext);

        // Initialize ClientAPI and TenantClientAPI
        ClientAPI clientAPI = generonClient.api(ClientAPI::new);
        TenantClientAPI tenantAPI = clientAPI.tenantAPI(1);

        // Run example
        HelloWorldServiceFactory helloWorldServiceFactory = tenantAPI.helloWorldServiceFactory();
        HelloWorldServiceBlockingAPI blockingApi = helloWorldServiceFactory.blocking();

        System.out.println("Result: " + blockingApi.hello()); // Prints "Result: Hello, Generon"

        // Close
        generonClient.close();
        generonNode.close();
    }

}
```

See the `HelloWorld` example for more details

## Project files

1. `build-config.json` is used to define client and/or server configuration for the code generator. It
   supports `client and server`, `client` or `server` only applications. `client and server` will usually not be used in
   the production and is made for testing purpose and easy setup. In real application it should be two separate projects
   when the `client` is usually a gateway, which wraps `Generon SDK` with HTTP or any other protocol. For the `client`
   it is enough to scan @Service interface, it does not need implementation. That's why all method configuration for the
   @Service is made inside the interface. Project can have "common" classes which will have all, including interfaces
   and be connected to server and client separately. Packages from libraries also can be included to the config. In
   theory `Cluster A` can be a client for `Cluster B`. It will allow such combinations too. But general advise is not to
   have such dependencies because it will add lots of complexities. It also has `saveRequest`, `saveCache`
   and `enableDataAlignment` configs. `saveRequest` will save json of a request payload which will be sent to the
   server. `saveCache` is used to save a json of the build cache. It will be saved anyway internally and used to deal
   with partial builds when only one or few classes will be compiled,recognized by annotation processor but each build
   requires a presence of all components. `enableDataAlignment` is a boolean value which means that protocol will be
   generated using memory alignment to avoid unaligned access by adding paddings to the offset before the read/write of
   each data type.
2. `generon.properties` - system and application properties. It can import other property files with defined override
   priority. There are multiple system properties which could be overridden. Can be used in combination
   with `@PropertyLoader` methods (from the @Configuration) which will load properties at runtime. Properties can be
   injected using `@Value` or `@ValueObject`.

## Common issues and how to resolve them

1. Seems like build is finished, but some files are not generated, why ?
   <br>
   Common reason is that `packages` from the `build-config.json` are configured incorrectly.
   If all was good before probably some classes were moved to other packages and `build-config.json` should be adjusted
   to use new package names.
   <br>
   Another reason is that some validations are not passed. Check logs and if there is an issue described it should be
   resolved first.
   <br>
   <br>
2. Serializable object field has `@StringType`, `@EnumType`, `@ObjectVersion` or another similar annotation, but seems
   like it's not considered by the system, why ?
   <br>
   Common reason is that these annotations are used directly on the field rather than within `@FromVersion`. These
   annotations should be related to the specific object version and will not work globally for the field.
   To define configuration use `@FromVersion(value = V, stringType = *, enumTypes = *, versions = *)`, where V is an
   object version. Use 1 to configure from the first version.
   <br>
   <br>
3. Run got `InaccessibleObjectException: Unable to make field private final ...`, how to fix ?
   <br>
   Add VM option `--add-opens java.base/sun.nio.ch=ALL-UNNAMED`