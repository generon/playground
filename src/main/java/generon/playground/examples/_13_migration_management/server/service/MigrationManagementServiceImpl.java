package generon.playground.examples._13_migration_management.server.service;

import generon.core.annotation.Inject;
import generon.core.annotation.MigrateTo;
import generon.core.annotation.SnapshotField;
import generon.integration.cluster.internal.component.MigrationComponent;
import generon.playground.examples._13_migration_management.common.event_producer.MigrationEventProducer;
import generon.playground.examples._13_migration_management.common.service.MigrationManagementService;
import generon.playground.examples._13_migration_management.common.type.SimpleEntity;
import io.aeron.cluster.service.Cluster;
import org.agrona.collections.Int2ObjectHashMap;

import java.util.ArrayList;
import java.util.List;

public class MigrationManagementServiceImpl implements MigrationManagementService {

    @Inject
    private Cluster cluster;

    @Inject
    private MigrationComponent migrationComponent;

    @Inject
    private MigrationEventProducer migrationEventProducer;

    @SnapshotField(1)
    private int entityIdCounter;

    @SnapshotField(value = 2, disabledFrom = 2)
    private final List<SimpleEntity> entityList = new ArrayList<>();

    @SnapshotField(value = 2, version = 2)
    private final Int2ObjectHashMap<SimpleEntity> entityMap = new Int2ObjectHashMap<>();

    @MigrateTo
    private void migrate(int version) {
        if (cluster.role() == Cluster.Role.LEADER) {
            // Do not print duplicate messages from other cluster members
            System.out.printf("Migration to version %d is triggered%n", version);
        }
    }

    @MigrateTo(2)
    private void migrateV2() {
        entityList.forEach(simpleEntity -> entityMap.put(simpleEntity.id, simpleEntity));
    }

    @Override
    public int createEntity(String name) {
        SimpleEntity simpleEntity = new SimpleEntity(++entityIdCounter, name);

        saveEntity(simpleEntity);
        triggerEvent(simpleEntity);

        return simpleEntity.id;
    }

    private void saveEntity(SimpleEntity simpleEntity) {
        // Use list for V1 and map for V2
        if (migrationComponent.currentRuntimeVersion() <= 1) {
            entityList.add(simpleEntity);
        } else {
            entityMap.put(simpleEntity.id, simpleEntity); // Use map for all versions starting from 2
        }
    }

    private void triggerEvent(SimpleEntity simpleEntity) {
        // Trigger eventV1 for V1 and eventV2 for V2
        if (migrationComponent.currentRuntimeVersion() <= 1) {
            migrationEventProducer.eventV1(simpleEntity.id);
        } else {
            migrationEventProducer.eventV2(simpleEntity.id, true);
        }
    }

    @Override
    public SimpleEntity getEntity(int id) {
        // Use different approaches to get user. Use list with O(N) for V1 and map with O(1) for V2
        if (migrationComponent.currentRuntimeVersion() <= 1) {
            for (SimpleEntity simpleEntity : entityList) {
                if (simpleEntity.id == id) {
                    return simpleEntity;
                }
            }

            return null;
        }

        return entityMap.get(id);
    }

}
