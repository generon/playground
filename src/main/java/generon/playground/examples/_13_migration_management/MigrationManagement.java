package generon.playground.examples._13_migration_management;

import generon.client.ClientAPI;
import generon.client.EventConsumerContext;
import generon.client.EventManager;
import generon.client.GlobalClientAPI;
import generon.client.TenantClientAPI;
import generon.client.admin_service.AdminServiceBlockingAPI;
import generon.client.migration_management_service.MigrationManagementServiceBlockingAPI;
import generon.playground.configuration.Example;
import generon.playground.examples._13_migration_management.client.event_listener.MigrationEventListenerImpl;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

public class MigrationManagement implements Example {

    @Override
    public void run(ClientAPI clientAPI) throws InterruptedException {
        GlobalClientAPI globalAPI = clientAPI.globalAPI();
        TenantClientAPI tenantAPI = clientAPI.tenantAPI(1);
        EventManager eventManager = clientAPI.eventManager();

        AdminServiceBlockingAPI adminServiceAPI = globalAPI.adminServiceFactory().blocking();
        MigrationManagementServiceBlockingAPI api = tenantAPI.migrationManagementServiceFactory().blocking();

        // Current version is 1. Will add to the list and trigger eventV1
        int entity1 = api.createEntity("Entity 1");
        System.out.println("Entity 1 (From list): " + api.getEntity(entity1));

        // Migrate. Set version 2
        System.out.println("Migrate. New runtime version: " + adminServiceAPI.migrate());

        // Current version is 2. Will add to the map and trigger eventV2
        System.out.println("Entity 1 (From map): " + api.getEntity(entity1));

        int entity2 = api.createEntity("Entity 2");
        System.out.println("Entity 2 (From map): " + api.getEntity(entity2));

        // Consume events
        CountDownLatch eventLatch = new CountDownLatch(2); // Await 2 events
        int consumerId = 1;

        EventConsumerContext eventConsumerContext = new EventConsumerContext.Builder(consumerId)
                .addListener(new MigrationEventListenerImpl(eventLatch))
                .build();

        eventManager.connectConsumer(eventConsumerContext);

        if (!eventLatch.await(5, TimeUnit.SECONDS)) {
            System.err.println("Event timeout");
        }

        eventManager.closeConsumer(consumerId);
    }

}
