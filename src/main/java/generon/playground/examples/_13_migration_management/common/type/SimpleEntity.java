package generon.playground.examples._13_migration_management.common.type;

import generon.core.annotation.scan.serializable.Serializable;

import java.util.StringJoiner;

@Serializable
public class SimpleEntity {

    public int id;
    public String name;

    public SimpleEntity() {
    }

    public SimpleEntity(int id, String name) {
        this.id = id;
        this.name = name;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", SimpleEntity.class.getSimpleName() + "[", "]")
                .add("id=" + id)
                .add("name='" + name + "'")
                .toString();
    }
}
