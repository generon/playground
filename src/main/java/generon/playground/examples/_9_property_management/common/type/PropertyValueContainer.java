package generon.playground.examples._9_property_management.common.type;

import generon.core.annotation.scan.serializable.Serializable;
import generon.playground.examples._9_property_management.common.type.value_object.DynamicValueObject;
import generon.playground.examples._9_property_management.common.type.value_object.StaticValueObject;

import java.time.Duration;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Serializable
public class PropertyValueContainer {

    public int primitive;
    public boolean[] booleanArray;
    public List<String> stringList;
    public Map<String, Integer> map;
    public Duration duration;
    public long defaultValue;

    public static PropertyValueContainer of(DynamicValueObject valueObject) {
        PropertyValueContainer propertyValueContainer = new PropertyValueContainer();

        propertyValueContainer.primitive = valueObject.primitive();
        propertyValueContainer.booleanArray = valueObject.booleanArray();
        propertyValueContainer.stringList = valueObject.stringList();
        propertyValueContainer.map = valueObject.map();
        propertyValueContainer.duration = valueObject.duration();
        propertyValueContainer.defaultValue = valueObject.defaultValue();

        return propertyValueContainer;
    }

    public static PropertyValueContainer of(StaticValueObject valueObject) {
        PropertyValueContainer propertyValueContainer = new PropertyValueContainer();

        propertyValueContainer.primitive = valueObject.primitive();
        propertyValueContainer.booleanArray = valueObject.booleanArray();
        propertyValueContainer.stringList = valueObject.stringList();
        propertyValueContainer.map = valueObject.map();
        propertyValueContainer.duration = valueObject.duration();
        propertyValueContainer.defaultValue = valueObject.defaultValue();

        return propertyValueContainer;
    }

    @Override
    public String toString() {
        return "PropertyValueContainer{" +
                "primitive=" + primitive +
                ", booleanArray=" + Arrays.toString(booleanArray) +
                ", stringList=" + stringList +
                ", map=" + map +
                ", duration=" + duration +
                ", defaultValue=" + defaultValue +
                '}';
    }

}
