package generon.playground.examples._9_property_management.common.service;

import generon.core.annotation.scan.service.Service;
import generon.core.annotation.scan.service.ServiceMethod;
import generon.playground.configuration.Constants;
import generon.playground.examples._9_property_management.common.type.PropertyValueContainer;

@Service(Constants.Id.Service.PROPERTY_MANAGEMENT)
public interface PropertyManagementService {

    @ServiceMethod(1)
    PropertyValueContainer dynamicProperties();

    @ServiceMethod(2)
    PropertyValueContainer staticProperties();

    @ServiceMethod(3)
    String appName();

    @ServiceMethod(4)
    String loadedProperty();

    @ServiceMethod(5)
    String url();

    @ServiceMethod(6)
    String profileValue();

}
