package generon.playground.examples._9_property_management;

import generon.client.ClientAPI;
import generon.client.TenantClientAPI;
import generon.client.property_management_service.PropertyManagementServiceBlockingAPI;
import generon.client.property_management_service.PropertyManagementServiceFactory;
import generon.playground.configuration.Example;

public class PropertyManagement implements Example {

    @Override
    public void run(ClientAPI clientAPI) {
        TenantClientAPI tenantAPI = clientAPI.tenantAPI(1);

        PropertyManagementServiceFactory propertyManagementServiceFactory = tenantAPI.propertyManagementServiceFactory();
        PropertyManagementServiceBlockingAPI api = propertyManagementServiceFactory.blocking();

        System.out.println("Dynamic properties: " + api.dynamicProperties());
        System.out.println("Static properties: " + api.staticProperties());
        System.out.println("App name: " + api.appName());
        System.out.println("Loaded property: " + api.loadedProperty());
        System.out.println("Url (with expressions): " + api.url());
        System.out.println("Value from 'test' profile: " + api.profileValue());
    }

}
