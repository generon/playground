Generon has advanced property management system which covers various use cases. It allows to inject properties, parse
and convert them to defined type, define property objects to resolve multiple properties within a prefix, generate
constants, resolve expressions and much more.

### Definition

Properties can be defined under `generon.properties`. We can import other files
via `file.import=/file1.properties,/file2.properties, ...`. Files can import other files recursively. Circular
dependencies are forbidden.

By default, duplicate keys are forbidden, and exception will be thrown. It can be allowed
by `config.property.key.allow-duplicate=true`. If set as `true` and duplicate keys are found we need to decide which
value to use:

- property: `config.property.key.priority-strategy.file`
- values: `file_root` or `file_import`
- default `file_root`

`file_root` means that values from parent (root) file will be used.
<br>
`file_import` means that values from child (imported) file will be used.

`@Configuration` class can have `@PropertyLoader` methods which can load properties at runtime when application
starts. It is useful when properties are hosted on separate or third party service. Methods must
return `Map<String, String>` which will represent property `key/value` pairs.

Also, we can define priority strategy between `property files` and `@PropertyLoad` methods.

- property: `config.property.key.priority-strategy.general`
- values: `file` or `runtime`
- default `file`

`file` means that values from files will be used.
<br>
`runtime` means that values loaded by `@PropertyLoader` will be used.

### Profiles

Generon supports profiles. We can define profiles using `-D` (VM options) or by environment variables. Key for profiles
is `generon.profile`, value can comtain multiple profiels sepaarted by comma. We can get profiles statically
from `EnvironmentContext.PROFILES` (Set<String>).

Generon will load and apply profile property file. Properties from the profile have priority and will override other
properties from `generon.properties`. Profile file name pattern is `generon-${profile}.properties`, for
example `generon-test.properties`, where `test` is a profile name. File will be loaded only if there is a profile
enabled. `@ValueObject` with constants will not see these profile properties because profile can not be determined at
compilation time. `PropertyManagement` example has property `profile-value` which will override default value
when `test` profile enabled.

### Annotations:

1. `@Value` (Field level). Allows to get property value by its key. We can define `Value.defaultValue` which will be
   used if key is not present. For example `@Value("my.prop1")` or `@Value(value = "my.prop1", defaultValue="42")`. If
   default value is not set and there is no property key found exception will be thrown.
2. `@ValueObject` (Class level). A record which combine `@Value` fields in single
   object. `@ValueObject("property.prefix")` can be used to define prefix for all fields. Each field can have `@Value`
   annotation to specify exact suffix of a property key. If `@Value` is not specified field name is used to determine
   property key, for example `myFieldName` -> `my.field.name`. `@ValueObject` has two configurations which can be
   defined by boolean `@ValueObject.asConstants`:
    1. `false`. Means `Dynamic` (default). Object can be injected via `@Inject`.
    2. `true`. Means `Static`. Will generate class with constants and can not be injected. Does not work
       with `@PropertyLoader` and `profile property files` because all values are resolved at compile time. We can
       use `@ValueObject.className` to define generated class name. If not specified it will take class name and
       add `Constants` suffix. Properties which are required to build class with constants will be sent to the server
       within request. Properties which are not related to `asConstants = true` will be filtered and not sent.

### Parsers & Converters

All property values are strings by its nature. And question is how to convert value to another type. Generon consider
type of `@Value` field and will parse/convert value to this type. Next types are supported:

1. Primitive
2. Boxed primitive (Same as primitive. Values are nullable)
3. Enum
4. String
5. Duration
6. Array, List, Set, Map of above types

Multilevel nested types like `List<String[]>` are not allowed. Values from `@Value.defaultValue` or `@PropertyLoader`
are working in the same way as `property file` values. For example, it can have map related value like `a:1,b:2` and be
parsed to `Map<String, Integer>` etc.

### Expressions

Generon supports expressions like `server.url=https://${server.host}:${server.port}`, which allows to reuse other
property values and avoid repetitions. Also, it has syntax for each parsed type:

1. Primitives. `boolean` - `false`/`true`, `byte/short/int/long` - `1`/`2`/... , `float/double` - `1.1`/`2.2`,
   char `a`/`b`.
2. Array, List, Set. `element1, element2, ...`.
3. Map. `a:1,b:2`, when `a` and `b` are keys, `1` and `2` are values.
4. Duration. `${number}${timeUnit}`. For example: `10m` (10 minutes), `100ms` (100 milliseconds).
   Here is a list of supported time units:
    - `ns` - `ChronoUnit.NANOS`
    - `us` - `ChronoUnit.MICROS`
    - `ms` - `ChronoUnit.MILLIS`
    - `s` - `ChronoUnit.SECONDS`
    - `m` - `ChronoUnit.MINUTES`
    - `h` - `ChronoUnit.HOURS`
    - `d` - `ChronoUnit.DAYS`

All types are validated, parsing works for each type separately, meaning we can have a `String` with `true`, `a:1,b:2`
etc. All expressions will work in the same way for `@Value.defaultValue`, `@PropertyLoader` as for `file` properties.