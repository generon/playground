package generon.playground.examples._9_property_management.server.configuration;

import generon.core.annotation.PropertyLoader;
import generon.core.annotation.scan.configuration.Configuration;

import java.util.Map;

@Configuration
public class PropertyConfiguration {

    @PropertyLoader
    public Map<String, String> propertyLoader() {
        return Map.of("loaded.property", "My loaded property2");
    }

}
