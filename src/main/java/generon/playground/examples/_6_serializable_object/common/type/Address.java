package generon.playground.examples._6_serializable_object.common.type;

import generon.core.annotation.scan.serializable.Serializable;
import generon.core.annotation.scan.serializable.Version;
import generon.core.annotation.scan.serializable.migration.field.FromVersion;
import generon.core.annotation.scan.serializable.migration.field.RemovedFromVersion;

@Serializable
public class Address {

    @Version
    public byte version;

    @FromVersion(1) // Can be removed because 1 is default
    @RemovedFromVersion(2)
    public String fullAddress;

    @FromVersion(2)
    public String country;

    @FromVersion(2)
    public String state;

    @FromVersion(2)
    public String city;

    @FromVersion(2)
    public String street;

    @FromVersion(2)
    public String postalCode;

    @FromVersion(2)
    public String buildingNumber;

    public static Address v1(String fullAddress) {
        Address address = new Address();
        address.version = 1;
        address.fullAddress = fullAddress;

        return address;
    }

    public static Address v2(String country, String state, String city, String street, String postalCode, String buildingNumber) {
        Address address = new Address();
        address.version = 2;
        address.country = country;
        address.state = state;
        address.city = city;
        address.street = street;
        address.postalCode = postalCode;
        address.buildingNumber = buildingNumber;

        return address;
    }

    @Override
    public String toString() {
        return "Address{" +
                "version=" + version +
                ", fullAddress='" + fullAddress + '\'' +
                ", country='" + country + '\'' +
                ", state='" + state + '\'' +
                ", city='" + city + '\'' +
                ", street='" + street + '\'' +
                ", postalCode='" + postalCode + '\'' +
                ", buildingNumber='" + buildingNumber + '\'' +
                '}';
    }
}
