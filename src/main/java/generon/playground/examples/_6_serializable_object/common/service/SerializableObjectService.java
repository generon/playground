package generon.playground.examples._6_serializable_object.common.service;

import generon.core.annotation.scan.serializable.migration.ReturnValueConfig;
import generon.core.annotation.scan.serializable.migration.object.ObjectVersion;
import generon.core.annotation.scan.service.Service;
import generon.core.annotation.scan.service.ServiceMethod;
import generon.playground.configuration.Constants;
import generon.playground.examples._6_serializable_object.common.type.Address;
import generon.playground.examples._6_serializable_object.common.type.User;

@Service(Constants.Id.Service.SERIALIZABLE_OBJECT)
public interface SerializableObjectService {

    @ServiceMethod(1)
    @ReturnValueConfig(versions = @ObjectVersion(1))
    User createUserV1(String name);

    @ServiceMethod(value = 1, version = 2)
    @ReturnValueConfig(versions = @ObjectVersion(2))
    User createUserV2(String name, String fullAddress);

    @ServiceMethod(value = 1, version = 3)
    @ObjectVersion(value = 1, type = Address.class)
    @ReturnValueConfig(versions = @ObjectVersion(3))
    User createUserV3(String name, Address address);

    @ServiceMethod(value = 1, version = 4)
    @ObjectVersion(value = 2, type = Address.class)
    @ReturnValueConfig(versions = @ObjectVersion(4))
    User createUserV4(String name, Address address);

    @ServiceMethod(value = 1, version = 5)
    @ReturnValueConfig(versions = @ObjectVersion(5))
    User createUserV5(String name, Address address);

    @ServiceMethod(2)
    @ObjectVersion(1)
    User createUserV1(User user);

    @ServiceMethod(value = 2, version = 2)
    @ObjectVersion(2)
    User createUserV2(User user);

    @ServiceMethod(value = 2, version = 3)
    @ObjectVersion(3)
    User createUserV3(User user);

    @ServiceMethod(value = 2, version = 4)
    @ObjectVersion(4)
    User createUserV4(User user);

    @ServiceMethod(value = 2, version = 5)
    @ObjectVersion(5)
    User createUserV5(User user);

    @ServiceMethod(3)
    User createUser(User user);

}
