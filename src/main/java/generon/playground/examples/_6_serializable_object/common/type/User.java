package generon.playground.examples._6_serializable_object.common.type;

import generon.core.annotation.scan.serializable.Serializable;
import generon.core.annotation.scan.serializable.Version;
import generon.core.annotation.scan.serializable.migration.field.FieldVersions;
import generon.core.annotation.scan.serializable.migration.field.FromVersion;
import generon.core.annotation.scan.serializable.migration.field.RemovedFromVersion;
import generon.core.annotation.scan.serializable.migration.object.ObjectVersion;

@Serializable
public class User {

    @Version
    public byte version;

    public long id;
    public String name;

    @FromVersion(2)
    @RemovedFromVersion(3)
    public String fullAddress;

    @FieldVersions(from = {
            @FromVersion(value = 3, versions = @ObjectVersion(1)),
            @FromVersion(value = 4, versions = @ObjectVersion(2)),
            @FromVersion(5) // Use dynamic Address version starting from User V5
    })
    public Address address;

    @Override
    public String toString() {
        return "User{" +
                "version=" + version +
                ", id=" + id +
                ", name='" + name + '\'' +
                ", fullAddress='" + fullAddress + '\'' +
                ", address=" + address +
                '}';
    }

}
