package generon.playground.examples._6_serializable_object;

import generon.client.ClientAPI;
import generon.client.TenantClientAPI;
import generon.client.serializable_object_service.SerializableObjectServiceBlockingAPI;
import generon.client.serializable_object_service.SerializableObjectServiceFactory;
import generon.playground.configuration.Example;
import generon.playground.examples._6_serializable_object.common.type.Address;
import generon.playground.examples._6_serializable_object.common.type.User;
import generon.playground.examples._6_serializable_object.common.type.address.AddressV1;
import generon.playground.examples._6_serializable_object.common.type.address.AddressV2;
import generon.playground.examples._6_serializable_object.common.type.user.UserV1;
import generon.playground.examples._6_serializable_object.common.type.user.UserV2;
import generon.playground.examples._6_serializable_object.common.type.user.UserV3;
import generon.playground.examples._6_serializable_object.common.type.user.UserV4;
import generon.playground.examples._6_serializable_object.common.type.user.UserV5;

public class SerializableObject implements Example {

    @Override
    public void run(ClientAPI clientAPI) {
        TenantClientAPI tenantAPI = clientAPI.tenantAPI(1);

        SerializableObjectServiceFactory serializableObjectServiceFactory = tenantAPI.serializableObjectServiceFactory();
        SerializableObjectServiceBlockingAPI api = serializableObjectServiceFactory.blocking();

        createUserV1_1(api);
        createUserV2_1(api);
        createUserV3_1(api);
        createUserV4_1(api);
        createUserV5_1(api);
        createUserV1_2(api);
        createUserV2_2(api);
        createUserV3_2(api);
        createUserV4_2(api);
        createUserV5_2(api);
        createUserV1to5(api);
    }

    private static void createUserV1_1(SerializableObjectServiceBlockingAPI api) {
        System.out.println("createUserV1_1: " + api.createUserV1("User V1_1"));
    }

    private static void createUserV2_1(SerializableObjectServiceBlockingAPI api) {
        System.out.println("createUserV2_1: " + api.createUserV2("User V2_1", "Full address. User:V2_1"));
    }

    private static void createUserV3_1(SerializableObjectServiceBlockingAPI api) {
        AddressV1 address = new AddressV1("Full address. User:V3_1");
        System.out.println("createUserV3_1: " + api.createUserV3("User V3_1", address));
    }

    private static void createUserV4_1(SerializableObjectServiceBlockingAPI api) {
        AddressV2 address = new AddressV2(
                "Generon. User:V4_1", "state", "city",
                "street", "postalCode", "buildingNumber"
        );

        System.out.println("createUserV4_1: " + api.createUserV4("User V4_1", address));
    }

    private static void createUserV5_1(SerializableObjectServiceBlockingAPI api) {
        Address addressV1 = Address.v1("Full address. User:V5_1");
        Address addressV2 = Address.v2("Generon. User:V5_1", "state", "city", "street", "postalCode", "buildingNumber");

        System.out.println("createUserV5_1. Address V1: " + api.createUserV5("User V5_1 1", addressV1));
        System.out.println("createUserV5_1. Address V2: " + api.createUserV5("User V5_1 2", addressV2));
    }

//    ---

    private static void createUserV1_2(SerializableObjectServiceBlockingAPI api) {
        UserV1 user = new UserV1(1001L, "User V1_2");
        System.out.println("createUserV1_2: " + api.createUserV1(user));
    }

    private static void createUserV2_2(SerializableObjectServiceBlockingAPI api) {
        UserV2 user = new UserV2(1002L, "User V2_2", "Full address. User:V2_2");
        System.out.println("createUserV2_2: " + api.createUserV2(user));
    }

    private static void createUserV3_2(SerializableObjectServiceBlockingAPI api) {
        AddressV1 address = new AddressV1("Full address. User:V3_2");
        UserV3 user = new UserV3(1003L, "User V3_2", address);

        System.out.println("createUserV3_2: " + api.createUserV3(user));
    }

    private static void createUserV4_2(SerializableObjectServiceBlockingAPI api) {
        AddressV2 address = new AddressV2(
                "Generon. User:V4_2", "state", "city",
                "street", "postalCode", "buildingNumber"
        );
        UserV4 user = new UserV4(1004L, "User V4_2", address);

        System.out.println("createUserV4_2: " + api.createUserV4(user));
    }

    private static void createUserV5_2(SerializableObjectServiceBlockingAPI api) {
        Address addressV1 = Address.v1("Full address. User:V5_2");
        Address addressV2 = Address.v2("Generon. User:V5_2", "state", "city", "street", "postalCode", "buildingNumber");

        UserV5 user1 = new UserV5(1001L, "User V5_2 1", addressV1);
        UserV5 user2 = new UserV5(1002L, "User V5_2 2", addressV2);

        System.out.println("createUserV5_2. Address V1: " + api.createUserV5(user1));
        System.out.println("createUserV5_2. Address V2: " + api.createUserV5(user2));
    }

    private static void createUserV1to5(SerializableObjectServiceBlockingAPI api) {
        User userV1 = new User();
        userV1.version = 1;
        userV1.id = 1003L;
        userV1.name = "User V1";

        User userV2 = new User();
        userV2.version = 2;
        userV2.id = 1004L;
        userV2.name = "User V2";
        userV2.fullAddress = "Full address. User:V2";

        User userV3 = new User();
        userV3.version = 3;
        userV3.id = 1005L;
        userV3.name = "User V3";

        // 'address.id = N' will be ignored and taken from @FieldVersions config based on user.version, which is 1
        userV3.address = Address.v1("Full address. User:V3");

        User userV4 = new User();
        userV4.version = 4;
        userV4.id = 1006L;
        userV4.name = "User V4";

        // 'address.id = N' will be ignored and taken from @FieldVersions config based on user.version, which is 2
        userV4.address = Address.v2("Generon. User:V4", "state", "city", "street", "postalCode", "buildingNumber");

        User userV5 = new User();
        userV5.version = 5;
        userV5.id = 1007L;
        userV5.name = "User V5";
        userV5.address = null;

        System.out.println("createUserV1to5. UserV1: " + api.createUser(userV1));
        System.out.println("createUserV1to5. UserV2: " + api.createUser(userV2));
        System.out.println("createUserV1to5. UserV3: " + api.createUser(userV3));
        System.out.println("createUserV1to5. UserV4: " + api.createUser(userV4));
        System.out.println("createUserV1to5. UserV5: " + api.createUser(userV5));
    }

}
