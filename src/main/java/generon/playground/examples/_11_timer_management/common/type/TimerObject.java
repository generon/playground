package generon.playground.examples._11_timer_management.common.type;

import generon.core.annotation.scan.serializable.Serializable;

@Serializable
public class TimerObject {

    public static final int INFINITE = -1;

    public long id;
    public long correlationId;
    public int amount;
    public long delay;
    public String payload;

    public TimerObject() {
    }

    public TimerObject(long id, int amount, long delay, String payload) {
        this.id = id;
        this.amount = amount;
        this.delay = delay;
        this.payload = payload;
    }

    public boolean isInfinite() {
        return amount == INFINITE;
    }

}
