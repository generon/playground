package generon.playground.examples._11_timer_management.common.service;

import generon.core.annotation.scan.service.Service;
import generon.core.annotation.scan.service.ServiceMethod;
import generon.playground.configuration.Constants;

@Service(Constants.Id.Service.TIMER_MANAGEMENT)
public interface TimeManagementService {

    @ServiceMethod(1)
    long scheduleInfinite(long delay, String payload);

    @ServiceMethod(2)
    long schedule(int amount, long delay, String payload);

    @ServiceMethod(3)
    boolean cancel(long timerId);

    @ServiceMethod(4)
    void cancelAll();

    @ServiceMethod(5)
    int totalTriggered();

}
