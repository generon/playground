package generon.playground.examples._11_timer_management.server.component;

import generon.core.annotation.Init;
import generon.core.annotation.Inject;
import generon.core.annotation.SnapshotField;
import generon.core.annotation.TimerEventHandler;
import generon.core.annotation.scan.component.Component;
import generon.integration.cluster.internal.component.TimerComponent;
import generon.playground.configuration.Constants;
import generon.playground.examples._11_timer_management.common.type.TimerObject;
import io.aeron.cluster.service.Cluster;
import org.agrona.collections.Long2ObjectHashMap;

@Component(Constants.Id.Component.TIMER_MANAGER)
public class TimerManager {

    @SnapshotField(1)
    private final Long2ObjectHashMap<TimerObject> timerByCorrelationId = new Long2ObjectHashMap<>();

    @SnapshotField(2)
    private long nextTimerId;

    @SnapshotField(3)
    private int timerCounter;

    @Inject
    private Cluster cluster;

    @Inject
    private TimerComponent timerComponent;

    private final Long2ObjectHashMap<TimerObject> timerById = new Long2ObjectHashMap<>();

    @Init
    private void init() {
        timerByCorrelationId.values().forEach(timerObject -> timerById.put(timerObject.id, timerObject));
    }

    @TimerEventHandler
    public void globalHandler(long correlationId, long timestamp) {
        timerCounter++; // Just count all timers
    }

    @TimerEventHandler
    public boolean chainHandler(long correlationId, long timestamp) {
        TimerObject timerObject = timerByCorrelationId.remove(correlationId);

        if (timerObject == null) {
            return false; // Skip, as it`s not our event
        }

        if (cluster.role() == Cluster.Role.LEADER) {
            // Do not print duplicate messages from other cluster members
            System.out.printf("Chain handler - %s, time - %d%n", timerObject.payload, timestamp);
        }

        if (!timerObject.isInfinite() && --timerObject.amount <= 0) {
            return true; // Do not reschedule a timer
        }

        // Reschedule timer. Correlation id is different each time
        timerObject.correlationId = correlationId = timerComponent.schedule(timerObject.delay);
        timerByCorrelationId.put(correlationId, timerObject);

        return true; // Next handlers in a chain will not be called
    }

    public long scheduleInfinite(long delay, String payload) {
        return schedule(TimerObject.INFINITE, delay, payload);
    }

    public long schedule(long delay, String payload) {
        return schedule(1, delay, payload);
    }

    public long schedule(int amount, long delay, String payload) {
        long correlationId = timerComponent.schedule(delay);

        TimerObject timerObject = new TimerObject(nextTimerId++, amount, delay, payload);

        timerById.put(timerObject.id, timerObject);
        timerByCorrelationId.put(correlationId, timerObject);

        return timerObject.id;
    }

    public boolean cancel(long timerId) {
        TimerObject timerObject = timerById.remove(timerId);

        if (timerObject == null) {
            return false;
        }

        timerComponent.cancel(timerObject.correlationId);
        timerByCorrelationId.remove(timerObject.correlationId);

        return true;
    }

    public void cancelAll() {
        Long2ObjectHashMap<TimerObject>.KeyIterator iterator = timerByCorrelationId.keySet().iterator();

        while (iterator.hasNext()) {
            long correlationId = iterator.nextLong();
            timerComponent.cancel(correlationId);
        }

        timerById.clear();
        timerByCorrelationId.clear();
    }

    public int timerCounter() {
        return timerCounter;
    }
}
