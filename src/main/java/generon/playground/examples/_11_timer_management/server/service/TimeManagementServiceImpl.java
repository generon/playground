package generon.playground.examples._11_timer_management.server.service;

import generon.core.annotation.Inject;
import generon.playground.examples._11_timer_management.common.service.TimeManagementService;
import generon.playground.examples._11_timer_management.server.component.TimerManager;

public class TimeManagementServiceImpl implements TimeManagementService {

    @Inject
    private TimerManager timerManager;

    @Override
    public long scheduleInfinite(long delay, String payload) {
        return timerManager.scheduleInfinite(delay, payload);
    }

    @Override
    public long schedule(int amount, long delay, String payload) {
        return timerManager.schedule(amount, delay, payload);
    }

    @Override
    public boolean cancel(long timerId) {
        return timerManager.cancel(timerId);
    }

    @Override
    public void cancelAll() {
        timerManager.cancelAll();
    }

    @Override
    public int totalTriggered() {
        return timerManager.timerCounter();
    }

}
