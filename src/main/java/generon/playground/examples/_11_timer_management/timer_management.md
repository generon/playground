### General overview

Schedulers can play a crustal role in the project design and architecture. They are used to execute some repetitive
tasks or one time tasks with some delay. It can be daily snapshots, batch jobs, statistics aggregators, Good-Till-Date
orders etc. When it comes to low-level systems and event sourcing, integrating a scheduler in deterministic way is a
challenge because all commands and timer events must be executed and replayed in the same exact order each time. Best is
when it is already solved and managed internally from the beginning. Aeron Cluster has this ability and Generon
integrates it to the own ecosystem.

### Schedule a task

We can not do something like `schedule(delay, () -> "do something useful")` because it is impossible to serialize lambda
and recover after restart. We need to use different approach - register timer by id first and handle it by id separately
when timer event arrives.
<br>
<br>
Generon has multiple internal components which can be injected and used in the code. One of them is `TimerComponent` and
has global scope. To register timer we can use `long correlationId = timerComponent.schedule(1000L)`, to
cancel - `timerComponent.cancel(correlationId)`. Method `schedule` has `long delay` argument, which is time in
milliseconds and returns `long correlationId`, which should be used later to `catch` the event when it is triggered.
When `schedule`method is called, after roughly `1000ms` timer with returned correlation id will be triggered. If we
need to have infinite timer which should repeat, we just need to reschedule new one each time during handling of
previous timer event. When node is restarted timers are initialized automatically.

### Handle timer event

To handle timer events we can define a method with `@TimerEventHandler` annotation at component level. This method must
be public and have next signature: `methodName(long correlationId, long timestamp)`. It can return `void`
or `boolean`. `void` means that it is usual timer which will catch all possible timer events. `boolean` means that this
timer is a part of chain responsibility pattern, if `true` is returned next handlers will not be called. If `false` is
returned next handler will be called. This optimization is good for case when we have many handlers but only one of them
will actually handle the event.
<br>
We can have multiple handlers. If handler does not recognize given `id` we can just skip it, because it may be related
to another handler. We can simply store correlation ids in a List or Set when we are scheduling a timer and when timer
is triggered check whether given id contains inside the collection to understand it is ours. Each `correlationId` is a
global and unique value among all schedules and handlers, meaning each handler should just ignore unknown ids without
errors. Alternatively, we can create a small abstraction layer to filter timer events by categories or timer types,
eliminating the need to ignore unknown ids in the handler itself.

### State and event producers

Timer can have own state. We just need to create separate component for the timer which will trigger and handle timer
events. We can add simple storage for the correlation ids or any other structure using `@SnapshotField` which will
ensure correct work after restarts. Also, we can inject our custom event producer into this timer component to trigger
some events whenever a timer event arrives. We can update a state of other components too because this behaviour is
fully deterministic. Order of schedules, handlers and other timings will be consistently maintained across all nodes and
during replays.

### Internals

`TimerComponent` uses `Cluster.time()` method which returns `processing time` of the current command. It will return
time of message event saved to Raft Log and will have same exact value each time and among all nodes. That's why
scheduling should be based on a delay rather than a specific timestamp. Internally, it will be scheduled for a time
calculated as `cluster.time() + delay`. Delay can be 0 or negative which means it will be triggered as soon as possible.
During replay when command time is in the past, timer event will be triggered immediately as usual
command. `Cluster.time()` should always be used instead of `System.currentTimeMillis()` and similar methods to achieve
determinism among all nodes in cluster and during replays.