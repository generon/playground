package generon.playground.examples._10_event_management.common.service;

import generon.core.annotation.scan.service.Service;
import generon.core.annotation.scan.service.ServiceMethod;
import generon.playground.configuration.Constants;

@Service(Constants.Id.Service.EVENT_MANAGEMENT)
public interface EventManagementService {

    @ServiceMethod(1)
    long create(String productName, double price);

    @ServiceMethod(2)
    void update(long productId, String newProductName, double newPrice);

    @ServiceMethod(3)
    void delete(long productId);

    @ServiceMethod(4)
    void stockAdd(long productId, int quantity);

    @ServiceMethod(5)
    void stockRemove(long productId, int quantity);

    @ServiceMethod(6)
    void discountApply(long productId, double discountAmount, String discountCode);

    @ServiceMethod(7)
    void discountRemove(long productId, String discountCode);

}
