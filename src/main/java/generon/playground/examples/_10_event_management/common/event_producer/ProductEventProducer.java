package generon.playground.examples._10_event_management.common.event_producer;

import generon.core.annotation.scan.event.EventMethod;
import generon.core.annotation.scan.event.EventProducer;
import generon.playground.configuration.Constants;

@EventProducer(value = Constants.Id.EventProducer.PRODUCT, streamId = Constants.EventStreamId.Product.CRUD)
public interface ProductEventProducer {

    /**
     * If EventMethod.streamId is not set EventProducer.streamId will be used instead.
     * The next three methods belong to the CRUD group and will use Constants.EventStreamId.Product.CRUD
     */
    @EventMethod(1)
    void created(long productId, String productName, double price);

    @EventMethod(2)
    void updated(long productId, String newProductName, double newPrice);

    @EventMethod(3)
    void deleted(long productId);

    /**
     * The next two methods belong to the stock group and use Constants.EventStreamId.Product.STOCK
     */
    @EventMethod(value = 4, streamId = Constants.EventStreamId.Product.STOCK)
    void stockAdded(long productId, int quantity);

    @EventMethod(value = 5, streamId = Constants.EventStreamId.Product.STOCK)
    void stockRemoved(long productId, int quantity);

    /**
     * The next two methods belong to the discount group and use Constants.EventStreamId.Product.DISCOUNT
     */
    @EventMethod(value = 6, streamId = Constants.EventStreamId.Product.DISCOUNT)
    void discountApplied(long productId, double discountAmount, String discountCode);

    @EventMethod(value = 7, streamId = Constants.EventStreamId.Product.DISCOUNT)
    void discountRemoved(long productId, String discountCode);

}