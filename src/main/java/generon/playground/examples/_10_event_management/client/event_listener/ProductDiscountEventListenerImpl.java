package generon.playground.examples._10_event_management.client.event_listener;

import generon.client.product_event_producer.ProductEventListener;
import generon.integration.cluster.objects.header.EventHeader;

public class ProductDiscountEventListenerImpl implements ProductEventListener {

    @Override
    public void onDiscountApplied(long productId, double discountAmount, String discountCode, EventHeader header) {
        System.out.println("Discount Listener. onDiscountApplied: productId: " + productId + ", discountAmount: " + discountAmount + ", discountCode: " + discountCode + ", timestamp: " + header.timestamp);
    }

    @Override
    public void onDiscountRemoved(long productId, String discountCode, EventHeader header) {
        System.out.println("Discount Listener. onDiscountRemoved: productId: " + productId + ", discountCode: " + discountCode + ", timestamp: " + header.timestamp);
    }

}
