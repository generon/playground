package generon.playground.examples._10_event_management.client.event_listener;

import generon.client.product_event_producer.ProductEventListener;
import generon.integration.cluster.objects.header.EventHeader;

public class ProductEventListenerImpl implements ProductEventListener {

    @Override
    public void onCreated(long productId, String productName, double price, EventHeader header) {
        System.out.println("Main Listener. onCreated: productId: " + productId + ", productName: " + productName + ", price: " + price + ", timestamp: " + header.timestamp);
    }

    @Override
    public void onUpdated(long productId, String newProductName, double newPrice, EventHeader header) {
        System.out.println("Main Listener. onUpdated: productId: " + productId + ", newProductName: " + newProductName + ", newPrice: " + newPrice + ", timestamp: " + header.timestamp);
    }

    @Override
    public void onDeleted(long productId, EventHeader header) {
        System.out.println("Main Listener. onDeleted: productId: " + productId + ", timestamp: " + header.timestamp);
    }

    @Override
    public void onStockAdded(long productId, int quantity, EventHeader header) {
        System.out.println("Main Listener. onStockAdded: productId: " + productId + ", quantity: " + quantity + ", timestamp: " + header.timestamp);
    }

    @Override
    public void onStockRemoved(long productId, int quantity, EventHeader header) {
        System.out.println("Main Listener. onStockRemoved: productId: " + productId + ", quantity: " + quantity + ", timestamp: " + header.timestamp);
    }

    @Override
    public void onDiscountApplied(long productId, double discountAmount, String discountCode, EventHeader header) {
        System.out.println("Main Listener. onDiscountApplied: productId: " + productId + ", discountAmount: " + discountAmount + ", discountCode: " + discountCode + ", timestamp: " + header.timestamp);
    }

    @Override
    public void onDiscountRemoved(long productId, String discountCode, EventHeader header) {
        System.out.println("Main Listener. onDiscountRemoved: productId: " + productId + ", discountCode: " + discountCode + ", timestamp: " + header.timestamp);
    }

}
