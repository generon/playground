package generon.playground.examples._10_event_management.server;

import generon.integration.config.ListenerPositionManager;
import generon.playground.configuration.Constants;

import java.util.concurrent.CountDownLatch;

public record CustomListenerPositionManager(
        long crudStreamPosition,
        boolean enablePersistLog,
        CountDownLatch eventLatch
) implements ListenerPositionManager {

    /**
     * Change position for the CRUD stream group only (for simplicity).
     * Real implementation should be able to save and recover all streams
     */
    @Override
    public long load(int streamId) {
        if (streamId == Constants.EventStreamId.Product.CRUD) {
            return crudStreamPosition;
        }

        return 0L;
    }

    @Override
    public void persist(int streamId, long position) {
        if (enablePersistLog) {
            System.out.println("Persist. streamId: " + streamId + ", position: " + position);
        }

        eventLatch.countDown();
    }

}