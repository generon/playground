package generon.playground.examples._10_event_management.server.service;

import generon.core.annotation.Inject;
import generon.core.annotation.SnapshotField;
import generon.playground.examples._10_event_management.common.event_producer.ProductEventProducer;
import generon.playground.examples._10_event_management.common.service.EventManagementService;

public class EventManagementServiceImpl implements EventManagementService {

    @Inject
    private ProductEventProducer productEventProducer;

    @SnapshotField(1)
    private int idCounter;

    @Override
    public long create(String name, double price) {
        int id = ++idCounter;

        productEventProducer.created(id, name, price);
        return id;
    }

    @Override
    public void update(long productId, String newProductName, double newPrice) {
        productEventProducer.updated(productId, newProductName, newPrice);
    }

    @Override
    public void delete(long productId) {
        productEventProducer.deleted(productId);
    }

    @Override
    public void stockAdd(long productId, int quantity) {
        productEventProducer.stockAdded(productId, quantity);
    }

    @Override
    public void stockRemove(long productId, int quantity) {
        productEventProducer.stockRemoved(productId, quantity);
    }

    @Override
    public void discountApply(long productId, double discountAmount, String discountCode) {
        productEventProducer.discountApplied(productId, discountAmount, discountCode);
    }

    @Override
    public void discountRemove(long productId, String discountCode) {
        productEventProducer.discountRemoved(productId, discountCode);
    }

}
