Recommended to read after `RootElementConfig`

### 1. Zero-Copy in Aeron: `offer` vs `tryClaim`

For data `writing` and `reading` Aeron has `Publication` and `Subscription` classes. Read is always zero-copy, so we
will focus on writing part.
<br>
<br>
Generon uses Aeron under the hood which has `Publication.offer` and `Publication.tryClaim` methods to publish data to
the stream. Both of these approaches have their pros and cons.

1. `Offer` is a standard way of sending data - you just give it, and Aeron does the rest. You don't have to worry
   about any of the tricky details.
2. `TryClaim` lets you write a message directly into Aeron's memory and achieve `zero-copy`. But there's a catch: you
   need to know how big your message is before you start writing, because it needs to reserve some space first. It does
   not support message split and message length is limited by MTU length which has 1408 bytes by default and is big
   enough for most cases. This can be adjusted and tuned, but to not make performance worse it requires clear
   understanding of what that is.

While `tryClaim` method is always faster for data transfer it also brings additional step which is message size
calculation. To have a benefit we should be able to do it very fast because otherwise `offer` with one more copy and
without size pre-calculation can be faster and should be used instead. It is always a trade-off and Generon knows which
one is better to use for specific message and set of types. It provides an adaptive approach, ensuring optimal
performance by analyzes data types and their configuration and make the best choice between `offer` and `tryClaim`.

### 2. Solution & Best Guess

We can always calculate size in advance, but the calculation can be time-consuming for some types. For example, we have
@ServiceMethod which has next signature - `myMethod(int intValue, long[] array)`. It means that on SDK side when we will
call this method we will write `int` and `long[]` to the `Publication`. In this case these two types requires `O(1)` to
calculate the message size. `int` is always `4 bytes` and `long[]` size is simply calculated like `array.length * 8`. To
calculate the size of entire message time complexity is `O(1)`, which means we can do it very fast and `tryClaim` is
what we need to use. In another case we could have `myMethod2(byte[] bytes, String[] strings)`. Here we can not
calculate entire message in `O(1)` time. byte[] as long[] has `O(1)`, but String[] does not. It requires iteration
through the collection to estimate its size because each element is different. In this case it is `O(N)`, and
using `offer` would be a better solution.
<br>
<br>
We can calculate sizes in constant time only for simple structures like primitives, single dimension primitive arrays,
String with ASCII encoding, Enums and @Serializable objects with such types (including nested). For two-dimensional
primitive arrays it will already be `O(N)` because we need iterate to know length of each subarray. One dimensional
array of boxed primitives also takes `O(N)` because we need to check each element whether it is null or not. Null header
takes 1 byte and in case of an Integer element, it can be 1 or 5 bytes. Same for all collections. Sure we could
calculate it for all types, but it usually does not worth it and instead of `tryClaim` we need to use `offer` which will
do one more copy, but also allow to skip size calculations. We could guess and set `tryClaim` size to some hardcoded
value like 512 bytes. It will ensure that we will have `zero-copy`, but if we exceed that amount we will get an
exception and if we will not fill all 512 bytes it means we are wasting space and performance also because these 512
bytes will be sent anyway. So it's always good to reserve the exact size or at least not much bigger one.

Generon automatically decides which approach to use. It scans all types and their configurations including nested
objects and checks whether all sizes can be calculated in `O(1)`. If so it will generate code which will generate code
to calculate message size and use `tryClaim`. If there is at least one element which needs `O(N)` to calculate the size
it will use `offer`. For example if we have a `@ServiceMethod` with 10 parameters, 9 of them needs `O(1)` but last one
is an object with nested one which has `int[][]` - we will use `offer` for entire message. If we remove that
field - `tryClaim` approach will be used.

`Root element configuration` also takes a place in Generon's decision.

1. `@StringType(ASCII)` will ensure that String have `O(1)`, for `UTF-8` case it is not possible because string length
   does not directly correlate with its size.
2. `@EnumType` is always `O(1)` even in case `STRING` type because it uses `ASCII` encoding internally.
3. `@ObjectVersion`. Each object version have own set fields and their configuration. Generon will consider only used
   fields of configured version and will check all fields only in case of `dynamic version` regardless, because version
   can not be determined at compile time.

### 3. Zero Copy with custom types integrated via TypeConverter or BufferedObject

By default, custom types will use `offer` method, but we can configure it to use `tryClaim` internally. To do this
we need to know data size in advance to make it possible.

1. For `TypeConverter` we need to add attribute `@TypeConverter(useZeroCopy = true)` and implementation `size`
   method, which has empty default implementation.
2. For `BufferObject` we need to implement `SizedWriter` instead of `Writer` and implement
   additional `int messageLength` method.