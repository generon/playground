package generon.playground.examples._4_type_support.server.service;

import generon.playground.examples._4_type_support.common.service.TypeSupportService;
import generon.playground.examples._4_type_support.common.type.AgronaTypesObject;
import generon.playground.examples._4_type_support.common.type.ComplexObject;
import generon.playground.examples._4_type_support.common.type.EnumType;
import generon.playground.examples._4_type_support.common.type.OtherTypesObject;
import generon.playground.examples._4_type_support.common.type.SimpleObject;
import generon.playground.examples._4_type_support.common.type.collection.PrimitiveListObject;
import generon.playground.examples._4_type_support.common.type.collection.PrimitiveMapObject;
import generon.playground.examples._4_type_support.common.type.collection.PrimitiveSetObject;
import generon.playground.examples._4_type_support.common.type.primitive.BoxedPrimitiveArrayObject;
import generon.playground.examples._4_type_support.common.type.primitive.BoxedPrimitiveObject;
import generon.playground.examples._4_type_support.common.type.primitive.PrimitiveArrayObject;
import generon.playground.examples._4_type_support.common.type.primitive.PrimitiveArrayObject2;
import generon.playground.examples._4_type_support.common.type.primitive.PrimitiveArrayObject3;
import generon.playground.examples._4_type_support.common.type.primitive.PrimitiveObject;
import org.agrona.collections.Int2IntHashMap;
import org.agrona.collections.Int2NullableObjectHashMap;
import org.agrona.collections.Int2ObjectHashMap;
import org.agrona.collections.IntArrayList;
import org.agrona.collections.IntHashSet;
import org.agrona.collections.Long2LongHashMap;
import org.agrona.collections.Long2NullableObjectHashMap;
import org.agrona.collections.Long2ObjectHashMap;
import org.agrona.collections.LongArrayList;
import org.agrona.collections.LongHashSet;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.SequencedCollection;
import java.util.Set;

public class TypeSupportServiceImpl implements TypeSupportService {

    // Primitive

    @Override
    public PrimitiveObject primitives(boolean booleanValue, byte byteValue, short shortValue, char charValue, float floatValue, int intValue, double doubleValue, long longValue) {
        return new PrimitiveObject(booleanValue, byteValue, shortValue, charValue, floatValue, intValue, doubleValue, longValue);
    }

    @Override
    public BoxedPrimitiveObject boxedPrimitives(Boolean booleanValue, Byte byteValue, Short shortValue, Character charValue, Float floatValue, Integer intValue, Double doubleValue, Long longValue) {
        return new BoxedPrimitiveObject(booleanValue, byteValue, shortValue, charValue, floatValue, intValue, doubleValue, longValue);
    }

    @Override
    public PrimitiveArrayObject primitiveArrays(boolean[] booleans, byte[] bytes, short[] shorts, char[] chars, float[] floats, int[] ints, double[] doubles, long[] longs) {
        return new PrimitiveArrayObject(booleans, bytes, shorts, chars, floats, ints, doubles, longs);
    }

    @Override
    public PrimitiveArrayObject2 primitiveArrays2(boolean[][] booleans, byte[][] bytes, short[][] shorts, char[][] chars, float[][] floats, int[][] ints, double[][] doubles, long[][] longs) {
        return new PrimitiveArrayObject2(booleans, bytes, shorts, chars, floats, ints, doubles, longs);
    }

    @Override
    public PrimitiveArrayObject3 primitiveArrays3(boolean[][][] booleans, byte[][][] bytes, short[][][] shorts, char[][][] chars, float[][][] floats, int[][][] ints, double[][][] doubles, long[][][] longs) {
        return new PrimitiveArrayObject3(booleans, bytes, shorts, chars, floats, ints, doubles, longs);
    }

    @Override
    public BoxedPrimitiveArrayObject boxedPrimitiveArrays(Boolean[] booleans, Byte[] bytes, Short[] shorts, Character[] chars, Float[] floats, Integer[] ints, Double[] doubles, Long[] longs) {
        return new BoxedPrimitiveArrayObject(booleans, bytes, shorts, chars, floats, ints, doubles, longs);
    }

    @Override
    public PrimitiveListObject primitiveLists(List<Boolean> booleans, List<Byte> bytes, List<Short> shorts, List<Character> chars, List<Float> floats, List<Integer> ints, List<Double> doubles, List<Long> longs) {
        return new PrimitiveListObject(booleans, bytes, shorts, chars, floats, ints, doubles, longs);
    }

    @Override
    public PrimitiveSetObject primitiveSets(Set<Boolean> booleans, Set<Byte> bytes, Set<Short> shorts, Set<Character> chars, Set<Float> floats, Set<Integer> ints, Set<Double> doubles, Set<Long> longs) {
        return new PrimitiveSetObject(booleans, bytes, shorts, chars, floats, ints, doubles, longs);
    }

    @Override
    public PrimitiveMapObject primitiveMaps(Map<Integer, Boolean> booleans, Map<Integer, Byte> bytes, Map<Integer, Short> shorts, Map<Integer, Character> chars, Map<Integer, Float> floats, Map<Integer, Integer> ints, Map<Integer, Double> doubles, Map<Integer, Long> longs) {
        return new PrimitiveMapObject(booleans, bytes, shorts, chars, floats, ints, doubles, longs);
    }

    // Object

    @Override
    public String string(String string) {
        return string;
    }

    @Override
    public EnumType enumValue(EnumType enumValue) {
        return enumValue;
    }

    @Override
    public SimpleObject simpleObject(SimpleObject object) {
        return object;
    }

    @Override
    public Map<String, SimpleObject> simpleObjectMap(Map<String, SimpleObject> map) {
        return map;
    }

    @Override
    public ComplexObject complexObject(ComplexObject object) {
        return object;
    }

    // Nested generic types

    @Override
    public Map<String, Map<Integer, SequencedCollection<ComplexObject[]>>> nestedGenericTypeMap(int entryAmount) {
        Map<String, Map<Integer, SequencedCollection<ComplexObject[]>>> result = new HashMap<>(entryAmount);

        for (int i = 0; i < entryAmount; i++) {
            ComplexObject[] array = new ComplexObject[2];
            array[0] = new ComplexObject("obj1_" + i, EnumType.VALUE_1, null);
            array[1] = new ComplexObject("obj2_" + i, EnumType.VALUE_2, List.of(new SimpleObject(i, null)));

            List<ComplexObject[]> list = new ArrayList<>();
            list.add(array);
            list.add(null);

            Map<Integer, SequencedCollection<ComplexObject[]>> map = new HashMap<>();
            map.put(i, list);

            result.put("key:" + i, map);
        }

        return result;
    }

    @Override
    public AgronaTypesObject agronaTypes(
            IntArrayList intArrayList, IntHashSet intHashSet, Int2IntHashMap int2IntHashMap, Int2ObjectHashMap<String> int2ObjectHashMap, Int2NullableObjectHashMap<String> int2NullableObjectHashMap,
            LongArrayList longArrayList, LongHashSet longHashSet, Long2LongHashMap long2LongHashMap, Long2ObjectHashMap<String> long2ObjectHashMap, Long2NullableObjectHashMap<String> long2NullableObjectHashMap
    ) {
        return new AgronaTypesObject(
                intArrayList, intHashSet, int2IntHashMap, int2ObjectHashMap, int2NullableObjectHashMap,
                longArrayList, longHashSet, long2LongHashMap, long2ObjectHashMap, long2NullableObjectHashMap
        );
    }

    @Override
    public OtherTypesObject otherTypes(BigDecimal bigDecimal, BigInteger bigInteger) {
        return new OtherTypesObject(bigDecimal, bigInteger);
    }

}
