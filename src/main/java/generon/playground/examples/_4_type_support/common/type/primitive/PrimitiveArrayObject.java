package generon.playground.examples._4_type_support.common.type.primitive;

import generon.core.annotation.scan.serializable.Serializable;

import java.util.Arrays;
import java.util.StringJoiner;

@Serializable
public class PrimitiveArrayObject {

    public boolean[] booleans;
    public byte[] bytes;
    public short[] shorts;
    public char[] chars;
    public float[] floats;
    public int[] ints;
    public double[] doubles;
    public long[] longs;

    public PrimitiveArrayObject() {
    }

    public PrimitiveArrayObject(boolean[] booleans, byte[] bytes, short[] shorts, char[] chars, float[] floats, int[] ints, double[] doubles, long[] longs) {
        this.booleans = booleans;
        this.bytes = bytes;
        this.shorts = shorts;
        this.chars = chars;
        this.floats = floats;
        this.ints = ints;
        this.doubles = doubles;
        this.longs = longs;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", PrimitiveArrayObject.class.getSimpleName() + "[", "]")
                .add("booleans=" + Arrays.toString(booleans))
                .add("bytes=" + Arrays.toString(bytes))
                .add("shorts=" + Arrays.toString(shorts))
                .add("chars=" + Arrays.toString(chars))
                .add("floats=" + Arrays.toString(floats))
                .add("ints=" + Arrays.toString(ints))
                .add("doubles=" + Arrays.toString(doubles))
                .add("longs=" + Arrays.toString(longs))
                .toString();
    }

}
