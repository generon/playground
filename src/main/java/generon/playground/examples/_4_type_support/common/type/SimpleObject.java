package generon.playground.examples._4_type_support.common.type;

import generon.core.annotation.scan.serializable.Serializable;

import java.util.Arrays;
import java.util.Objects;
import java.util.StringJoiner;

@Serializable
public class SimpleObject {

    public int intValue;
    public byte[] bytes;

    public SimpleObject() {
    }

    public SimpleObject(int intValue, byte[] bytes) {
        this.intValue = intValue;
        this.bytes = bytes;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SimpleObject that = (SimpleObject) o;
        return intValue == that.intValue && Arrays.equals(bytes, that.bytes);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(intValue);
        result = 31 * result + Arrays.hashCode(bytes);
        return result;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", SimpleObject.class.getSimpleName() + "[", "]")
                .add("intValue=" + intValue)
                .add("bytes=" + Arrays.toString(bytes))
                .toString();
    }

}
