package generon.playground.examples._4_type_support.common.type.collection;

import generon.core.annotation.scan.serializable.Serializable;

import java.util.List;
import java.util.StringJoiner;

@Serializable
public class PrimitiveListObject {

    public List<Boolean> booleans;
    public List<Byte> bytes;
    public List<Short> shorts;
    public List<Character> chars;
    public List<Float> floats;
    public List<Integer> ints;
    public List<Double> doubles;
    public List<Long> longs;

    public PrimitiveListObject() {
    }

    public PrimitiveListObject(List<Boolean> booleans, List<Byte> bytes, List<Short> shorts, List<Character> chars, List<Float> floats, List<Integer> ints, List<Double> doubles, List<Long> longs) {
        this.booleans = booleans;
        this.bytes = bytes;
        this.shorts = shorts;
        this.chars = chars;
        this.floats = floats;
        this.ints = ints;
        this.doubles = doubles;
        this.longs = longs;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", PrimitiveListObject.class.getSimpleName() + "[", "]")
                .add("booleans=" + booleans)
                .add("bytes=" + bytes)
                .add("shorts=" + shorts)
                .add("chars=" + chars)
                .add("floats=" + floats)
                .add("ints=" + ints)
                .add("doubles=" + doubles)
                .add("longs=" + longs)
                .toString();
    }

}
