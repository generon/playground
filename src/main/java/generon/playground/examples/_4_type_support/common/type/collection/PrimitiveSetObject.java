package generon.playground.examples._4_type_support.common.type.collection;

import generon.core.annotation.scan.serializable.Serializable;

import java.util.Set;
import java.util.StringJoiner;

@Serializable
public class PrimitiveSetObject {

    public Set<Boolean> booleans;
    public Set<Byte> bytes;
    public Set<Short> shorts;
    public Set<Character> chars;
    public Set<Float> floats;
    public Set<Integer> ints;
    public Set<Double> doubles;
    public Set<Long> longs;

    public PrimitiveSetObject() {
    }

    public PrimitiveSetObject(Set<Boolean> booleans, Set<Byte> bytes, Set<Short> shorts, Set<Character> chars, Set<Float> floats, Set<Integer> ints, Set<Double> doubles, Set<Long> longs) {
        this.booleans = booleans;
        this.bytes = bytes;
        this.shorts = shorts;
        this.chars = chars;
        this.floats = floats;
        this.ints = ints;
        this.doubles = doubles;
        this.longs = longs;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", PrimitiveSetObject.class.getSimpleName() + "[", "]")
                .add("booleans=" + booleans)
                .add("bytes=" + bytes)
                .add("shorts=" + shorts)
                .add("chars=" + chars)
                .add("floats=" + floats)
                .add("ints=" + ints)
                .add("doubles=" + doubles)
                .add("longs=" + longs)
                .toString();
    }

}
