package generon.playground.examples._4_type_support.common.type;

import generon.core.annotation.scan.serializable.Serializable;

import java.util.List;
import java.util.StringJoiner;

@Serializable
public class ComplexObject {

    public String string;
    public EnumType enumValue;
    public List<SimpleObject> subObjects;

    public ComplexObject() {
    }

    public ComplexObject(String string, EnumType enumValue, List<SimpleObject> subObjects) {
        this.string = string;
        this.enumValue = enumValue;
        this.subObjects = subObjects;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", ComplexObject.class.getSimpleName() + "[", "]")
                .add("string='" + string + "'")
                .add("enumValue=" + enumValue)
                .add("subObjects=" + subObjects)
                .toString();
    }

}
