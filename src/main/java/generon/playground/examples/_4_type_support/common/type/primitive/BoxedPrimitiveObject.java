package generon.playground.examples._4_type_support.common.type.primitive;

import generon.core.annotation.scan.serializable.Serializable;

import java.util.StringJoiner;

@Serializable
public class BoxedPrimitiveObject {

    public Boolean booleanValue;
    public Byte byteValue;
    public Short shortValue;
    public Character charValue;
    public Float floatValue;
    public Integer intValue;
    public Double doubleValue;
    public Long longValue;

    public BoxedPrimitiveObject() {
    }

    public BoxedPrimitiveObject(Boolean booleanValue, Byte byteValue, Short shortValue, Character charValue, Float floatValue, Integer intValue, Double doubleValue, Long longValue) {
        this.booleanValue = booleanValue;
        this.byteValue = byteValue;
        this.shortValue = shortValue;
        this.charValue = charValue;
        this.floatValue = floatValue;
        this.intValue = intValue;
        this.doubleValue = doubleValue;
        this.longValue = longValue;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", BoxedPrimitiveObject.class.getSimpleName() + "[", "]")
                .add("booleanValue=" + booleanValue)
                .add("byteValue=" + byteValue)
                .add("shortValue=" + shortValue)
                .add("charValue=" + charValue)
                .add("floatValue=" + floatValue)
                .add("intValue=" + intValue)
                .add("doubleValue=" + doubleValue)
                .add("longValue=" + longValue)
                .toString();
    }

}
