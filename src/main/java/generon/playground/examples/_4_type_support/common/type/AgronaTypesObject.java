package generon.playground.examples._4_type_support.common.type;

import generon.core.annotation.scan.serializable.Serializable;
import org.agrona.collections.Int2IntHashMap;
import org.agrona.collections.Int2NullableObjectHashMap;
import org.agrona.collections.Int2ObjectHashMap;
import org.agrona.collections.IntArrayList;
import org.agrona.collections.IntHashSet;
import org.agrona.collections.Long2LongHashMap;
import org.agrona.collections.Long2NullableObjectHashMap;
import org.agrona.collections.Long2ObjectHashMap;
import org.agrona.collections.LongArrayList;
import org.agrona.collections.LongHashSet;

@Serializable
public class AgronaTypesObject {

    public IntArrayList intArrayList;
    public IntHashSet intHashSet;
    public Int2IntHashMap int2IntHashMap;
    public Int2ObjectHashMap<String> int2ObjectHashMap;
    public Int2NullableObjectHashMap<String> int2NullableObjectHashMap;

    public LongArrayList longArrayList;
    public LongHashSet longHashSet;
    public Long2LongHashMap long2LongHashMap;
    public Long2ObjectHashMap<String> long2ObjectHashMap;
    public Long2NullableObjectHashMap<String> long2NullableObjectHashMap;

    public AgronaTypesObject() {
    }

    public AgronaTypesObject(
            IntArrayList intArrayList, IntHashSet intHashSet, Int2IntHashMap int2IntHashMap, Int2ObjectHashMap<String> int2ObjectHashMap, Int2NullableObjectHashMap<String> int2NullableObjectHashMap,
            LongArrayList longArrayList, LongHashSet longHashSet, Long2LongHashMap long2LongHashMap, Long2ObjectHashMap<String> long2ObjectHashMap, Long2NullableObjectHashMap<String> long2NullableObjectHashMap
    ) {
        this.intArrayList = intArrayList;
        this.intHashSet = intHashSet;
        this.int2IntHashMap = int2IntHashMap;
        this.int2ObjectHashMap = int2ObjectHashMap;
        this.int2NullableObjectHashMap = int2NullableObjectHashMap;

        this.longArrayList = longArrayList;
        this.longHashSet = longHashSet;
        this.long2LongHashMap = long2LongHashMap;
        this.long2ObjectHashMap = long2ObjectHashMap;
        this.long2NullableObjectHashMap = long2NullableObjectHashMap;
    }

    @Override
    public String toString() {
        return "AgronaTypesObject{" +
                "intArrayList=" + intArrayList +
                ", intHashSet=" + intHashSet +
                ", int2IntHashMap=" + int2IntHashMap +
                ", int2ObjectHashMap=" + int2ObjectHashMap +
                ", int2NullableObjectHashMap=" + int2NullableObjectHashMap +
                ", longArrayList=" + longArrayList +
                ", longHashSet=" + longHashSet +
                ", long2LongHashMap=" + long2LongHashMap +
                ", long2ObjectHashMap=" + long2ObjectHashMap +
                ", long2NullableObjectHashMap=" + long2NullableObjectHashMap +
                '}';
    }

}
