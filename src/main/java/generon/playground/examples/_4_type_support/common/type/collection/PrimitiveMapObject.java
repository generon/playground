package generon.playground.examples._4_type_support.common.type.collection;

import generon.core.annotation.scan.serializable.Serializable;

import java.util.Map;
import java.util.StringJoiner;

@Serializable
public class PrimitiveMapObject {

    public Map<Integer, Boolean> booleans;
    public Map<Integer, Byte> bytes;
    public Map<Integer, Short> shorts;
    public Map<Integer, Character> chars;
    public Map<Integer, Float> floats;
    public Map<Integer, Integer> ints;
    public Map<Integer, Double> doubles;
    public Map<Integer, Long> longs;

    public PrimitiveMapObject() {
    }

    public PrimitiveMapObject(Map<Integer, Boolean> booleans, Map<Integer, Byte> bytes, Map<Integer, Short> shorts, Map<Integer, Character> chars, Map<Integer, Float> floats, Map<Integer, Integer> ints, Map<Integer, Double> doubles, Map<Integer, Long> longs) {
        this.booleans = booleans;
        this.bytes = bytes;
        this.shorts = shorts;
        this.chars = chars;
        this.floats = floats;
        this.ints = ints;
        this.doubles = doubles;
        this.longs = longs;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", PrimitiveMapObject.class.getSimpleName() + "[", "]")
                .add("booleans=" + booleans)
                .add("bytes=" + bytes)
                .add("shorts=" + shorts)
                .add("chars=" + chars)
                .add("floats=" + floats)
                .add("ints=" + ints)
                .add("doubles=" + doubles)
                .add("longs=" + longs)
                .toString();
    }

}
