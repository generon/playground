package generon.playground.examples._4_type_support.common.type.primitive;

import generon.core.annotation.scan.serializable.Serializable;

import java.util.StringJoiner;

@Serializable
public class PrimitiveObject {

    public boolean booleanValue;
    public byte byteValue;
    public short shortValue;
    public char charValue;
    public float floatValue;
    public int intValue;
    public double doubleValue;
    public long longValue;

    public PrimitiveObject() {
    }

    public PrimitiveObject(boolean booleanValue, byte byteValue, short shortValue, char charValue, float floatValue, int intValue, double doubleValue, long longValue) {
        this.booleanValue = booleanValue;
        this.byteValue = byteValue;
        this.shortValue = shortValue;
        this.charValue = charValue;
        this.floatValue = floatValue;
        this.intValue = intValue;
        this.doubleValue = doubleValue;
        this.longValue = longValue;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", PrimitiveObject.class.getSimpleName() + "[", "]")
                .add("booleanValue=" + booleanValue)
                .add("byteValue=" + byteValue)
                .add("shortValue=" + shortValue)
                .add("charValue=" + charValue)
                .add("floatValue=" + floatValue)
                .add("intValue=" + intValue)
                .add("doubleValue=" + doubleValue)
                .add("longValue=" + longValue)
                .toString();
    }

}
