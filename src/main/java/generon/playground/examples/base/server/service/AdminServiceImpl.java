package generon.playground.examples.base.server.service;

import generon.core.annotation.Inject;
import generon.integration.cluster.internal.component.AdminComponent;
import generon.playground.examples.base.common.service.AdminService;
import org.agrona.collections.IntArrayList;

public class AdminServiceImpl implements AdminService {

    @Inject
    private AdminComponent adminComponent;

    @Override
    public void takeSnapshot() {
        adminComponent.takeSnapshot();
    }

    @Override
    public int migrate() {
        return adminComponent.migrate();
    }

    @Override
    public int currentRuntimeVersion() {
        return adminComponent.currentRuntimeVersion();
    }

    @Override
    public int lastCodeVersion() {
        return adminComponent.lastCodeVersion();
    }

    @Override
    public int currentCodeVersion() {
        return adminComponent.currentCodeVersion();
    }

    @Override
    public IntArrayList tenantIds() {
        return adminComponent.tenantIds();
    }

    @Override
    public int addTenant() {
        return adminComponent.addTenant();
    }

    @Override
    public boolean removeTenant(int tenantId) {
        return adminComponent.removeTenant(tenantId);
    }

    @Override
    public void await() {
    }

}
