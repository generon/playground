package generon.playground.examples.base.common.service;

import generon.core.annotation.scan.Scope;
import generon.core.annotation.scan.service.Service;
import generon.core.annotation.scan.service.ServiceMethod;
import generon.playground.configuration.Constants;
import org.agrona.collections.IntArrayList;

@Service(value = Constants.Id.Service.ADMIN, scope = Scope.GLOBAL)
public interface AdminService {

    @ServiceMethod(1)
    void takeSnapshot();

    @ServiceMethod(2)
    int migrate();

    @ServiceMethod(3)
    int currentRuntimeVersion();

    @ServiceMethod(4)
    int lastCodeVersion();

    @ServiceMethod(5)
    int currentCodeVersion();

    @ServiceMethod(6)
    IntArrayList tenantIds();

    @ServiceMethod(7)
    int addTenant();

    @ServiceMethod(8)
    boolean removeTenant(int tenantId);

    @ServiceMethod(9)
    void await();

}
