package generon.playground.examples._8_dependency_injection.server.configuration;

import generon.core.annotation.Init;
import generon.core.annotation.Inject;
import generon.core.annotation.scan.configuration.Configuration;
import generon.core.annotation.scan.configuration.injection.InjectionElement;
import io.aeron.cluster.service.Cluster;

import java.time.DayOfWeek;
import java.time.Duration;
import java.util.List;

@Configuration
public class InjectionConfiguration {

    @Inject
    private Cluster cluster;

    private long startupTime = -1L;

    @InjectionElement(qualifier = "weekends")
    public List<DayOfWeek> weekends() {
        return List.of(DayOfWeek.SUNDAY, DayOfWeek.SATURDAY);
    }

    @InjectionElement(qualifier = "workdays")
    public List<DayOfWeek> workdays() {
        return List.of(DayOfWeek.MONDAY, DayOfWeek.THURSDAY, DayOfWeek.WEDNESDAY, DayOfWeek.THURSDAY, DayOfWeek.FRIDAY);
    }

    @InjectionElement
    public List<DayOfWeek> allWeekDays() {
        return List.of(DayOfWeek.values());
    }

    @InjectionElement(qualifier = "same_qualifier")
    public int primitiveInt() {
        return 10;
    }

    @InjectionElement(qualifier = "same_qualifier")
    public Integer boxedInt() {
        return 10;
    }

    @InjectionElement(qualifier = "same_qualifier")
    public String string() {
        return "Nice string value";
    }

    @InjectionElement
    public Duration duration() {
        return Duration.ofSeconds(10L);
    }

    @Init
    private void init() {
        startupTime = cluster.time();
    }

    public long startupTime() {
        return startupTime;
    }
}
