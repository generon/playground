package generon.playground.examples._15_multitenancy.common.value_object;

import generon.core.annotation.Value;
import generon.core.annotation.scan.value.ValueObject;

@ValueObject
public record TenantValueObject(@Value("tenant_name") String tenantName) {
}
