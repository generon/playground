package generon.playground.examples._15_multitenancy.common.service;

import generon.core.annotation.scan.Scope;
import generon.core.annotation.scan.service.Service;
import generon.core.annotation.scan.service.ServiceMethod;
import generon.playground.configuration.Constants;
import org.agrona.collections.Int2ObjectHashMap;

@Service(value = Constants.Id.Service.GLOBAL_SCOPE, scope = Scope.GLOBAL)
public interface GlobalScopeService {

    @ServiceMethod(1)
    Int2ObjectHashMap<String> namesFromTenantScopeHook();

    @ServiceMethod(2)
    Int2ObjectHashMap<String> namesFromGlobalScopeHooks();

    @ServiceMethod(3)
    Int2ObjectHashMap<String> namesFromTenantScopeProperty();

    @ServiceMethod(4)
    Int2ObjectHashMap<String> namesFromTenantScopePropertyObject();

    @ServiceMethod(5)
    Int2ObjectHashMap<String> namesFromTenantScopeInitHook();

    @ServiceMethod(6)
    Int2ObjectHashMap<String> namesTakenByTenantId();

    void addFromTenantScope(int tenantId);

    void removeFromTenantScope(int tenantId);

}
