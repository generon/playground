package generon.playground.examples._15_multitenancy;

import generon.client.ClientAPI;
import generon.client.GlobalClientAPI;
import generon.client.TenantClientAPI;
import generon.client.admin_service.AdminServiceBlockingAPI;
import generon.client.global_scope_service.GlobalScopeServiceBlockingAPI;
import generon.client.tenant_scope_service.TenantScopeServiceBlockingAPI;
import generon.playground.configuration.ConfigurableExample;
import generon.playground.configuration.TestConfigurator;

public class Multitenancy implements ConfigurableExample {

    @Override
    public void run(ClientAPI clientAPI) {
        GlobalClientAPI globalAPI = clientAPI.globalAPI();

        AdminServiceBlockingAPI adminServiceAPI = globalAPI.adminServiceFactory().blocking();
        GlobalScopeServiceBlockingAPI globalScopeServiceAPI = globalAPI.globalScopeServiceFactory().blocking();

        // Add second tenant and print all tenant ids
        System.out.println("New tenant created: " + adminServiceAPI.addTenant());
        printTenantNames(globalScopeServiceAPI);

        TenantClientAPI tenant1API = clientAPI.tenantAPI(1);
        TenantClientAPI tenant2API = clientAPI.tenantAPI(2);

        TenantScopeServiceBlockingAPI tenantScopeService1 = tenant1API.tenantScopeServiceFactory().blocking();
        TenantScopeServiceBlockingAPI tenantScopeService2 = tenant2API.tenantScopeServiceFactory().blocking();

        System.out.println("Global value from Tenant 1: " + tenantScopeService1.globalScopeString());
        System.out.println("Global value from Tenant 2: " + tenantScopeService2.globalScopeString());

        tenantScopeService1.addNumber(1);
        tenantScopeService1.addNumber(2);

        tenantScopeService2.addNumber(3);
        tenantScopeService2.addNumber(4);

        System.out.println("Tenant 1 state: " + tenantScopeService1.getNumbers()); // 1, 2
        System.out.println("Tenant 2 state: " + tenantScopeService2.getNumbers()); // 3, 4

        // Add first tenant and print all tenant ids
        adminServiceAPI.removeTenant(1);
        System.out.println("Tenant 1 removed");
        printTenantNames(globalScopeServiceAPI);

        try {
            tenantScopeService1.getNumbers();
        } catch (Exception e) {
            System.out.println("Expected exception. Can not call api of tenant 1: " + e.getMessage());
        }

        System.out.println("Tenant 2 state: " + tenantScopeService2.getNumbers());

        adminServiceAPI.removeTenant(2); // Remove last tenant
        System.out.println("Tenant 2 removed");

        try {
            tenantScopeService2.getNumbers();
        } catch (Exception e) {
            System.out.println("Expected exception. Can not call api of tenant 2: " + e.getMessage());
        }

        System.out.println("Active tenantIds: " + adminServiceAPI.tenantIds()); // No tenants at all. ID counter is reset to 1
        System.out.println("New tenant created: " + adminServiceAPI.addTenant()); // Add tenant with id 1

        // Reuse same instance 'tenantScopeService1'
        System.out.println("Tenant 1 state: " + tenantScopeService1.getNumbers()); // Empty
    }

    /**
     * Disable error handler. It is used to remove expected exception stack trace from the console.
     * Do not do this in real life:)
     */
    @Override
    public TestConfigurator testConfigurator() {
        return new TestConfigurator().disableErrorLogs();
    }

    private static void printTenantNames(GlobalScopeServiceBlockingAPI globalScopeServiceAPI) {
        System.out.println("Tenant names. Approach 1: " + globalScopeServiceAPI.namesFromTenantScopeHook());
        System.out.println("Tenant names. Approach 2: " + globalScopeServiceAPI.namesFromGlobalScopeHooks());
        System.out.println("Tenant names. Approach 3: " + globalScopeServiceAPI.namesFromTenantScopeProperty());
        System.out.println("Tenant names. Approach 4: " + globalScopeServiceAPI.namesFromTenantScopePropertyObject());
        System.out.println("Tenant names. Approach 5: " + globalScopeServiceAPI.namesFromTenantScopeInitHook());
        System.out.println("Tenant names. Approach 6: " + globalScopeServiceAPI.namesTakenByTenantId());
    }

}
