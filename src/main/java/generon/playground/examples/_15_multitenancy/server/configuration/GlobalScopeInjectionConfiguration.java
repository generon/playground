package generon.playground.examples._15_multitenancy.server.configuration;

import generon.core.annotation.scan.Scope;
import generon.core.annotation.scan.configuration.Configuration;
import generon.core.annotation.scan.configuration.injection.InjectionElement;

@Configuration(scope = Scope.GLOBAL)
public class GlobalScopeInjectionConfiguration {

    @InjectionElement(qualifier = "global_scope")
    public String globalScopeString() {
        return "Generon Framework";
    }

}
