package generon.playground.examples._15_multitenancy.server.service;

import generon.core.annotation.BeforeTenantRemoved;
import generon.core.annotation.Inject;
import generon.core.annotation.OnTenantAdded;
import generon.core.annotation.SnapshotField;
import generon.core.annotation.Value;
import generon.integration.cluster.internal.component.TenantComponent;
import generon.integration.injection.TenantScope;
import generon.playground.examples._15_multitenancy.common.service.GlobalScopeService;
import generon.playground.examples._15_multitenancy.common.value_object.TenantValueObject;
import generon.playground.examples._15_multitenancy.server.component.TenantNameComponent;
import org.agrona.collections.Int2ObjectHashMap;

public class GlobalScopeServiceImpl implements GlobalScopeService {

    @Inject
    private TenantComponent tenantComponent;

    @Inject
    private TenantScope<TenantNameComponent> tenantScopeMetadata;

    @Inject
    private TenantScope<TenantValueObject> tenantValueObject;

    @Value("tenant_name")
    private TenantScope<String> tenantNameProperty;

    /**
     * Stateful. Updated only when tenant is added or removed, requires @SnapshotField to be saved
     */
    @SnapshotField(1)
    private final Int2ObjectHashMap<String> namesFromTenantScopeHook = new Int2ObjectHashMap<>();

    /**
     * Stateful. Updated only when tenant is added or removed, required @SnapshotField to be saved
     */
    @SnapshotField(2)
    private final Int2ObjectHashMap<String> namesFromGlobalScopeHooks = new Int2ObjectHashMap<>();

    /**
     * Flyweight. Reset and initialized on each call
     */
    private final Int2ObjectHashMap<String> namesFromTenantScopeProperty = new Int2ObjectHashMap<>();
    private final Int2ObjectHashMap<String> namesFromTenantScopePropertyObject = new Int2ObjectHashMap<>();
    private final Int2ObjectHashMap<String> namesFromTenantScopeInitHook = new Int2ObjectHashMap<>();
    private final Int2ObjectHashMap<String> namesTakenByTenantId = new Int2ObjectHashMap<>();

    /**
     * When tenant is created or removed tenant itself will notify GlobalService to update the map.
     * Map always will have the latest state.
     */
    @Override
    public Int2ObjectHashMap<String> namesFromTenantScopeHook() {
        return namesFromTenantScopeHook;
    }

    /**
     * When tenant is created or removed global scope itself will react on hooks to update the map.
     * Map always will have the latest state.
     */
    @Override
    public Int2ObjectHashMap<String> namesFromGlobalScopeHooks() {
        return namesFromGlobalScopeHooks;
    }

    /**
     * Uses 'TenantScope<String> tenantNameProperty', which contains tenant name loaded from tenant properties.
     * to returns get tenantId and name from. Final map is recalculated on each call.
     * We also could have '@Value("tenant_name") String tenantName' in the tenant scope and then trigger method of
     * GlobalService to save the state, similar to approach 1
     */
    @Override
    public Int2ObjectHashMap<String> namesFromTenantScopeProperty() {
        namesFromTenantScopeProperty.clear();
        tenantNameProperty.forEach(namesFromTenantScopeProperty::put);

        return namesFromTenantScopeProperty;
    }

    /**
     * Uses 'TenantScope<TenantValueObject> tenantValueObject', which contains tenant property object with its name
     * loaded from tenant properties. Final map is recalculated each time.
     * We also could have '@Inject TenantValueObject valueObject' in the tenant scope and then trigger method of
     * GlobalService to save the state, similar to approach 1
     */
    @Override
    public Int2ObjectHashMap<String> namesFromTenantScopePropertyObject() {
        namesFromTenantScopePropertyObject.clear();
        tenantValueObject.forEach((tenantId, valueObject) -> namesFromTenantScopePropertyObject.put(tenantId, valueObject.tenantName()));

        return namesFromTenantScopePropertyObject;
    }

    /**
     * Uses 'TenantScope<TenantNameComponent> tenantScopeMetadata', which always has the latest state of tenants,
     * to get tenantId and name from. Final map is recalculated each time.
     */
    @Override
    public Int2ObjectHashMap<String> namesFromTenantScopeInitHook() {
        namesFromTenantScopeInitHook.clear();
        tenantScopeMetadata.forEach((tenantId, component) -> namesFromTenantScopeInitHook.put(tenantId, component.tenantName()));

        return namesFromTenantScopeInitHook;
    }

    /**
     * Uses 'tenantComponent.tenantIds()' which returns current tenant ids and 'tenantScopeMetadata.type(tenantId)'
     * to get name from. Final map is recalculated on each call.
     */
    @Override
    public Int2ObjectHashMap<String> namesTakenByTenantId() {
        namesTakenByTenantId.clear();

        tenantComponent.tenantIds().forEachInt(tenantId -> {
            TenantNameComponent tenantNameComponent = tenantScopeMetadata.type(tenantId);
            String tenantName = tenantNameComponent.tenantName();

            namesTakenByTenantId.put(tenantId, tenantName);
        });

        return namesTakenByTenantId;
    }

    @Override
    public void addFromTenantScope(int tenantId) {
        TenantNameComponent tenantNameComponent = tenantScopeMetadata.type(tenantId);
        String tenantName = tenantNameComponent.tenantName();

        namesFromTenantScopeHook.put(tenantId, tenantName);
    }

    @Override
    public void removeFromTenantScope(int tenantId) {
        namesFromTenantScopeHook.remove(tenantId);
    }

    // Global scope hooks. Methods require 'int tenantId' parameter
    @OnTenantAdded
    private void onTenantAdded(int tenantId) {
        namesFromGlobalScopeHooks.put(tenantId, tenantScopeMetadata.type(tenantId).tenantName());
    }

    @BeforeTenantRemoved
    private void beforeTenantRemoved(int tenantId) {
        namesFromGlobalScopeHooks.remove(tenantId);
    }

}
