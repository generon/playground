package generon.playground.examples._5_custom_type_support.server.service;

import generon.playground.examples._5_custom_type_support.common.service.CustomTypeService;
import generon.playground.examples._5_custom_type_support.common.type.buffer_object.InstantBO;
import generon.playground.examples._5_custom_type_support.common.type.buffer_object.Product;
import generon.playground.examples._5_custom_type_support.common.type.buffer_object.ProductBO;

import java.time.Instant;
import java.util.List;

public class CustomTypeServiceImpl implements CustomTypeService {

    private final InstantBO instantBO = new InstantBO(); // Flyweight

    @Override
    public Instant instantTC(Instant instant) {
        return instant;
    }

    @Override
    public InstantBO instantBO(InstantBO instantBO) {
        return instantBO;
    }

    @Override
    public ProductBO productBO(ProductBO productBO) {
        return productBO;
    }

    @Override
    public InstantBO timestampToInstantBO(Long timestamp) {
        InstantBO.Writer writer = instantBO.writer();

        if (timestamp == null) {
            writer.wrapAsNull();
            return instantBO;
        }

        writer.wrap(timestamp);
        return instantBO;
    }

    @Override
    public Long instantBOToTimestamp(InstantBO instantBO) {
        InstantBO.Reader reader = instantBO.reader();

        if (reader.timestamp == -1L) {
            return null;
        }

        return reader.timestamp;
    }

    @Override
    public Instant instantBOToInstant(InstantBO instantBO) {
        InstantBO.Reader reader = instantBO.reader();
        return reader.asInstant();
    }

    @Override
    public Product productBOToProduct(ProductBO productBO) {
        ProductBO.Reader reader = productBO.reader();
        return reader.asProduct();
    }

    @Override
    public List<Instant> genericInstant(List<InstantBO> list) {
        return list.stream()
                .map(bo -> bo.reader().asInstant())
                .toList();
    }

    @Override
    public List<Product> genericProduct(List<ProductBO> list) {
        return list.stream()
                .map(bo -> bo.reader().asProduct())
                .toList();
    }

}
