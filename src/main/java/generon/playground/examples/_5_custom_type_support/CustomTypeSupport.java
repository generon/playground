package generon.playground.examples._5_custom_type_support;

import generon.client.ClientAPI;
import generon.client.TenantClientAPI;
import generon.client.custom_type_service.CustomTypeServiceBlockingAPI;
import generon.client.custom_type_service.CustomTypeServiceFactory;
import generon.playground.configuration.Example;
import generon.playground.examples._5_custom_type_support.common.type.buffer_object.InstantBO;
import generon.playground.examples._5_custom_type_support.common.type.buffer_object.Product;
import generon.playground.examples._5_custom_type_support.common.type.buffer_object.ProductBO;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

public class CustomTypeSupport implements Example {

    private static final InstantBO INSTANT_BO = new InstantBO();
    private static final ProductBO PRODUCT_BO = new ProductBO();

    @Override
    public void run(ClientAPI clientAPI) {
        TenantClientAPI tenantAPI = clientAPI.tenantAPI(1);

        CustomTypeServiceFactory customTypeServiceFactory = tenantAPI.customTypeServiceFactory();
        CustomTypeServiceBlockingAPI api = customTypeServiceFactory.blocking();

        instantTC(api);
        instantBO(api);
        productBO(api);
        timestampToInstantBO(api);
        instantBOToTimestamp(api);
        instantBOToInstant(api);
        genericInstant(api);
        genericProduct(api);
    }

    private static void instantTC(CustomTypeServiceBlockingAPI api) {
        System.out.println("instantTC: " + api.instantTC(Instant.now()));
        System.out.println("instantTC (null): " + api.instantTC(null));
    }

    private static void instantBO(CustomTypeServiceBlockingAPI api) {
        INSTANT_BO.writer().wrap(Instant.now());
        InstantBO response = api.instantBO(INSTANT_BO);
        System.out.println("instantBO: " + response.reader().asInstant());

        INSTANT_BO.writer().wrapAsNull();
        InstantBO response2 = api.instantBO(INSTANT_BO);
        System.out.println("instantBO (null): " + response2.reader().asInstant());
    }

    private static void productBO(CustomTypeServiceBlockingAPI api) {
        PRODUCT_BO.writer().wrap(1, "Product Name 1", 100L);
        ProductBO response = api.productBO(PRODUCT_BO);
        System.out.println("productBO: " + response.reader().asProduct());

        PRODUCT_BO.writer().wrapAsNull();
        ProductBO response2 = api.productBO(PRODUCT_BO);
        System.out.println("productBO (null): " + response2.reader().asProduct());
    }

    private static void timestampToInstantBO(CustomTypeServiceBlockingAPI api) {
        InstantBO response = api.timestampToInstantBO(Instant.now().toEpochMilli());
        System.out.println("timestampToInstantBO: " + response.reader().asInstant());

        InstantBO response2 = api.timestampToInstantBO(null);
        System.out.println("timestampToInstantBO (null): " + response2.reader().asInstant());
    }

    private static void instantBOToTimestamp(CustomTypeServiceBlockingAPI api) {
        INSTANT_BO.writer().wrap(Instant.now());
        Long response = api.instantBOToTimestamp(INSTANT_BO);
        System.out.println("instantBOToTimestamp: " + response);

        INSTANT_BO.writer().wrapAsNull();
        Long response2 = api.instantBOToTimestamp(INSTANT_BO);
        System.out.println("instantBOToTimestamp (null): " + response2);
    }

    private static void instantBOToInstant(CustomTypeServiceBlockingAPI api) {
        INSTANT_BO.writer().wrap(Instant.now());
        Instant response = api.instantBOToInstant(INSTANT_BO);
        System.out.println("instantBOToInstant: " + response);

        INSTANT_BO.writer().wrapAsNull();
        Instant response2 = api.instantBOToInstant(INSTANT_BO);
        System.out.println("instantBOToInstant (null): " + response2);
    }

    private static void genericInstant(CustomTypeServiceBlockingAPI api) {
        InstantBO instantBO1 = new InstantBO();
        InstantBO instantBO2 = new InstantBO();
        InstantBO instantBO3 = new InstantBO();

        instantBO1.writer().wrapAsNull();
        instantBO2.writer().wrap(Instant.now());
        instantBO3.writer().wrap(Instant.now().plus(1, ChronoUnit.HOURS));

        List<Instant> response = api.genericInstant(List.of(instantBO1, instantBO2, instantBO3));
        System.out.println("genericInstant: " + response);
    }

    private static void genericProduct(CustomTypeServiceBlockingAPI api) {
        ProductBO productBO1 = new ProductBO();
        ProductBO productBO2 = new ProductBO();
        ProductBO productBO3 = new ProductBO();

        productBO1.writer().wrapAsNull();
        productBO2.writer().wrap(1, "Product Name 1", 100L);
        productBO3.writer().wrap(2, "Product Name 2", 200L);

        List<Product> response = api.genericProduct(List.of(productBO1, productBO2, productBO3));
        System.out.println("genericProduct: " + response);
    }

}
