### Problem

@Serializable object can also be considered as a custom type, but it can contain only known types or other @Serializable
objects. What if we want to integrate a type which already exist in JDK or another library, and we can not or do not
want to modify this class ? That`s why we need to have a possibility to write something own and deed it to the Generon
Protocol.

### Solution

`TypeConverter` and `BufferObject` are the two ways of declaring custom type:

1. `TypeConverter`. We can create a class with `@TypeConverter` which implements `TypeConverterTemplate<T>` interface
   with read/write methods, where `T` is type we want to integrate. It is relatively simple and registers type directly.
   `write()` method should return new offset. `read()` method returns `T`. `offset()` method should return new offset
   after `read`.
2. `BufferObject` interface is designed to wrap a buffer segment and have full control over it. BufferObject itself
   should never be null as parameter or value - meaning call `api.instantBO_1(null)` is invalid and will throw NPE.
   BufferObject has reader and writer interfaces. Two implementations should be in one object due to inability to split
   it for case with `@SnapshotField`, we want field to be defined as single direct type. If we are sending data - use
   writer, if we are receiving - reader.

Note! Type converter and buffer object have read/write methods which should return new offset. Both approaches must be
implemented very carefully. It should read and write same amount of data and be consistent, otherwise it will corrupt a
type and all other types under the same message. For example if something wrong happened entire API request or response,
event read or write, snapshot take or apply will be entirely wrong because next process in a sequence will may have
wrong offset and point to the wrong data which makes it impossible to proceed. It will lead to unexpected behavior
or/and even worse - no exceptions can be thrown at all and data will be wrongly represented. Serialization /
Deserialization code should be tested with 100% coverage, because described issues are not acceptable.