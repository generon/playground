package generon.playground.examples._5_custom_type_support.common.type.buffer_object;

import generon.core.annotation.scan.serializable.Serializable;

import java.util.StringJoiner;

@Serializable
public class Product {

    public int id;
    public String name;
    public long price;

    @Override
    public String toString() {
        return new StringJoiner(", ", Product.class.getSimpleName() + "[", "]")
                .add("id=" + id)
                .add("name='" + name + "'")
                .add("price=" + price)
                .toString();
    }
}
