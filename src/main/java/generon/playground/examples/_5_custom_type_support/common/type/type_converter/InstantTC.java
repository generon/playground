package generon.playground.examples._5_custom_type_support.common.type.type_converter;

import generon.core.annotation.scan.TypeConverter;
import generon.core.objects.type_converter.TypeConverterTemplate;
import org.agrona.DirectBuffer;
import org.agrona.MutableDirectBuffer;

import java.time.Instant;

@TypeConverter(useZeroCopy = true)
public class InstantTC implements TypeConverterTemplate<Instant> {

    private int offset = 0;

    @Override
    public int write(Instant data, MutableDirectBuffer buffer, int offset) {
        buffer.putLong(offset, data == null ? -1L : data.toEpochMilli());
        offset += 8;

        this.offset = offset;
        return offset;
    }

    @Override
    public Instant read(DirectBuffer buffer, int offset) {
        long timestamp = buffer.getLong(offset);
        offset += 8;

        this.offset = offset;
        return timestamp == -1 ? null : Instant.ofEpochMilli(timestamp);
    }

    @Override
    public int size(Instant data) {
        return Long.BYTES; // 8
    }

    @Override
    public int offset() {
        return offset;
    }

}