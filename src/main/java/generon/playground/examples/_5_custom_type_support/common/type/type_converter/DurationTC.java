package generon.playground.examples._5_custom_type_support.common.type.type_converter;

import generon.core.annotation.scan.TypeConverter;
import generon.core.objects.type_converter.TypeConverterTemplate;
import org.agrona.DirectBuffer;
import org.agrona.MutableDirectBuffer;

import java.time.Duration;

@TypeConverter(useZeroCopy = true)
public class DurationTC implements TypeConverterTemplate<Duration> {

    private int offset = 0;

    @Override
    public int write(Duration data, MutableDirectBuffer buffer, int offset) {
        long value = data == null ? -1L : data.toMillis();
        buffer.putLong(offset, value);

        this.offset = offset;
        return offset();
    }

    @Override
    public Duration read(DirectBuffer buffer, int offset) {
        long value = buffer.getLong(offset);
        Duration duration = value == -1L ? null : Duration.ofMillis(value);

        this.offset = offset;
        return duration;
    }

    @Override
    public int size(Duration data) {
        return 8;
    }

    @Override
    public int offset() {
        return offset;
    }
}
