package generon.playground.examples._5_custom_type_support.common.type.buffer_object;

import generon.core.objects.buffer.BufferObject;
import org.agrona.DirectBuffer;
import org.agrona.MutableDirectBuffer;

import java.time.Instant;

public class InstantBO implements BufferObject<InstantBO.Reader, InstantBO.Writer> {

    private Reader reader;
    private Writer writer;

    @Override
    public Reader reader() {
        return reader == null ? (reader = new Reader(writer)) : reader;
    }

    @Override
    public Writer writer() {
        return writer == null ? (writer = new Writer(reader)) : writer;
    }

    public static class Reader implements BufferObject.Reader {

        public long timestamp;

        public Reader(Writer writer) {
            if (writer == null) {
                return;
            }

            this.timestamp = writer.timestamp;
        }

        @Override
        public int read(DirectBuffer buffer, int offset) {
            this.timestamp = buffer.getLong(offset);
            offset += 8;

            return offset;
        }

        public Instant asInstant() {
            if (this.timestamp == -1L) {
                return null;
            }

            return Instant.ofEpochMilli(timestamp);
        }

    }

    public static class Writer implements BufferObject.SizedWriter {

        private long timestamp;

        public Writer(Reader reader) {
            if (reader == null) {
                return;
            }

            this.timestamp = reader.timestamp;
        }

        public Writer wrapAsNull() {
            this.timestamp = -1L;
            return this;
        }

        public Writer wrap(long timestamp) {
            this.timestamp = timestamp;
            return this;
        }

        public Writer wrap(Instant instant) {
            this.timestamp = instant == null ? -1L : instant.toEpochMilli();
            return this;
        }

        @Override
        public int write(MutableDirectBuffer buffer, int offset) {
            buffer.putLong(offset, this.timestamp);
            offset += 8;

            return offset;
        }

        @Override
        public int messageLength() {
            return Long.BYTES; // 8
        }

    }

}
