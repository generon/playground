package generon.playground.examples._5_custom_type_support.common.service;

import generon.core.annotation.scan.service.Service;
import generon.core.annotation.scan.service.ServiceMethod;
import generon.playground.configuration.Constants;
import generon.playground.examples._5_custom_type_support.common.type.buffer_object.InstantBO;
import generon.playground.examples._5_custom_type_support.common.type.buffer_object.Product;
import generon.playground.examples._5_custom_type_support.common.type.buffer_object.ProductBO;

import java.time.Instant;
import java.util.List;

@Service(Constants.Id.Service.CUSTOM_TYPE)
public interface CustomTypeService {

    @ServiceMethod(1)
    Instant instantTC(Instant instant);

    @ServiceMethod(2)
    InstantBO instantBO(InstantBO instantBO);

    @ServiceMethod(3)
    ProductBO productBO(ProductBO productBO);

    @ServiceMethod(4)
    InstantBO timestampToInstantBO(Long timestamp);

    @ServiceMethod(5)
    Long instantBOToTimestamp(InstantBO instantBO);

    @ServiceMethod(6)
    Instant instantBOToInstant(InstantBO instantBO);

    @ServiceMethod(7)
    Product productBOToProduct(ProductBO productBO);

    @ServiceMethod(8)
    List<Instant> genericInstant(List<InstantBO> instantBOList);

    @ServiceMethod(9)
    List<Product> genericProduct(List<ProductBO> products);

}
