package generon.playground.examples._5_custom_type_support.common.type.buffer_object;

import generon.core.objects.buffer.BufferObject;
import generon.utils.ByteConstants;
import org.agrona.DirectBuffer;
import org.agrona.MutableDirectBuffer;

public class ProductBO implements BufferObject<ProductBO.Reader, ProductBO.Writer> {

    private Reader reader;
    private Writer writer;

    private final Product product = new Product(); // // Flyweight. Fields can be flattened to avoid this object

    @Override
    public Reader reader() {
        return reader == null ? (reader = new Reader(product)) : reader;
    }

    @Override
    public Writer writer() {
        return writer == null ? (writer = new Writer(product)) : writer;
    }

    public static class Reader implements BufferObject.Reader {

        public boolean isNull;
        private final Product product;

        public Reader(Product product) {
            this.product = product;
        }

        @Override
        public int read(DirectBuffer buffer, int offset) {
            if (buffer.getByte(offset++) == ByteConstants.B0) {
                this.isNull = true;
                return offset;
            }

            this.isNull = false;

            product.id = buffer.getInt(offset);
            offset += 4;

            int nameHeader = buffer.getInt(offset);
            if (nameHeader != -1) {
                product.name = buffer.getStringUtf8(offset, nameHeader);
                offset += nameHeader + 4;
            } else {
                product.name = null;
                offset += 4;
            }

            product.price = buffer.getLong(offset);
            offset += 8;

            return offset;
        }

        public Product asProduct() {
            if (isNull) {
                return null;
            }

            return product;
        }

    }

    public static class Writer implements BufferObject.Writer {

        private boolean isNull;
        private final Product product;

        public Writer(Product product) {
            this.product = product;
        }

        public Writer wrapAsNull() {
            this.isNull = true;
            return this;
        }

        public Writer wrap(int id, String name, long price) {
            this.isNull = false;

            product.id = id;
            product.name = name;
            product.price = price;

            return this;
        }

        @Override
        public int write(MutableDirectBuffer buffer, int offset) {
            if (isNull) {
                buffer.putByte(offset++, ByteConstants.B0);
                return offset;
            }

            buffer.putByte(offset++, ByteConstants.B1);
            buffer.putInt(offset, product.id);
            offset += 4;

            String name = product.name;
            if (name != null) {
                offset += buffer.putStringUtf8(offset, name);
            } else {
                buffer.putInt(offset, -1);
                offset += 4;
            }

            buffer.putLong(offset, product.price);
            offset += 8;

            return offset;
        }

    }

}
