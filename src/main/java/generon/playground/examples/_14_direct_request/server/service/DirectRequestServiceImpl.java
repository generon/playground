package generon.playground.examples._14_direct_request.server.service;

import generon.core.annotation.SnapshotField;
import generon.playground.examples._14_direct_request.common.service.DirectRequestService;
import org.agrona.collections.IntArrayList;

public class DirectRequestServiceImpl implements DirectRequestService {

    @SnapshotField(1)
    private final IntArrayList intArrayList = new IntArrayList();

    @Override
    public void addNumber(int number) {
        intArrayList.add(number);
    }

    @Override
    public IntArrayList getNumbers() {
        return intArrayList;
    }
}
