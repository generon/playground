package generon.playground.examples._14_direct_request;

import generon.client.ClientAPI;
import generon.client.GlobalClientAPI;
import generon.client.TenantClientAPI;
import generon.client.admin_service.AdminServiceBlockingAPI;
import generon.client.admin_service.AdminServiceFactory;
import generon.client.direct_request_service.DirectRequestServiceAPI;
import generon.client.direct_request_service.DirectRequestServiceBlockingAPI;
import generon.client.direct_request_service.DirectRequestServiceFactory;
import generon.playground.configuration.Example;
import generon.playground.configuration.Utils;
import org.agrona.collections.IntArrayList;

public class DirectRequest implements Example {

    @Override
    public void run(ClientAPI clientAPI) {
        GlobalClientAPI globalAPI = clientAPI.globalAPI();
        TenantClientAPI tenantAPI = clientAPI.tenantAPI(1);

        AdminServiceFactory adminServiceFactory = globalAPI.adminServiceFactory();
        DirectRequestServiceFactory directRequestServiceFactory = tenantAPI.directRequestServiceFactory();

        AdminServiceBlockingAPI adminServiceAPI = adminServiceFactory.blocking();
        DirectRequestServiceAPI noReplyAPI = directRequestServiceFactory.noReply();
        DirectRequestServiceBlockingAPI blockingAPI = directRequestServiceFactory.blocking();

        noReplyAPI.addNumber(1);
        noReplyAPI.addNumber(2);

        IntArrayList numbers = loadNumbersDirectly(adminServiceAPI, blockingAPI);
        System.out.println(numbers);
    }

    /**
     * Note! In this example it is possible to load empty list, because direct request is processed immediately comparing to usual command and most probably will respond faster.
     * Thread inside the node can receive direct call earlier, meaning data can be not yet available.
     * Even when we do "await" which will go through Raft Log and be replicated it still can return empty list.
     * The reason is simple. Await will respond when majority processed command, not waiting for all.
     * Direct call can go the that node which is not processed "await" command yet. Retry is made for demo purpose only to always return a value.
     * It should not be used in real code at all, because it is completely normal behavior.
     */
    private IntArrayList loadNumbersDirectly(AdminServiceBlockingAPI adminAPI, DirectRequestServiceBlockingAPI directCallAPI) {
        final int maxRetries = 3;

        for (int retry = 0; retry < maxRetries; retry++) {
            adminAPI.await(); // Await for majority to process all commands
            IntArrayList numbers = directCallAPI.getNumbers();

            if (numbers != null) {
                return numbers;
            }

            if (retry != maxRetries - 1) {
                Utils.sleep(100);
            }
        }

        return null;
    }

}
