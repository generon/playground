### General overview

APIs are used for few obvious reasons:

1. Save some data to get it later
2. Trigger some action which will change the state
3. Do both above and return something at the same time
4. Read data and not change state at all

First three are changing the state, while last one is not.

In the cluster all nodes are replicating a state from a leader in deterministic manner which ensures that all nodes have
exactly same state. State is eventually consistent, meaning that all nodes will not have same state immediately but with
some time and usually the difference is less than 1 millisecond and not noticeable. Leader is always ahead of the
followers, but it is also eventually consistent because it will not change its state immediately after command received.
It will wait for acknowledgment and only then process received command.

Client is usually communicates only with a leader. That`s why leader is usually a most loaded node in the cluster. When
client is sending command to the leader it will be saved to the Raft Log on the disk, be replicated to all followers and
when leader received acknowledgment from the at least half of them, to get majority, it will process command and send a
response. Also, it will process it during replay, because it is part of the Raft Log. But not all API calls will lead to
the state change. Read only requests may happen many times more often than write requests, which changes state. It means
there is a lot of work done for nothing and cluster looses performance by doing redundant work.

For read only request we can call specific node directly, leader or follower. Direct call means loading data directly
from the memory of the node, avoiding all redundant steps, which is the fastest way possible to get the data. There is
no much difference which node to pick for read only request, but knowing that leader is a most loaded node it is better
to bother followers. Even if read only request is heavy, and it takes some time to process this request it will not slow
down cluster at all, because leader will not wait for all, but only for majority in cluster. For example
there are 3 nodes, leader will wait only for 1 follower to respond, which means it does not care how much another
follower is behind in processing.

It brings us to the powerful and elegant solution, which is using followers directly for read only requests and improve
performance of the cluster overall.

### Solution

We can mark each API method as read only using `@ServiceMethod(value = N, readOnly = true)`. That`s it.

Generon will generate different communication channel, determine followers and use them when client calls this API. It
will use round-robin to distribute load among all followers, handle reconnections and leader reelections. When leader is
gone down and cluster did leader reelection it will detect it stop using new leader, which was follower before, and
consider old leader as follower. In this case most probably old leader is not active, but once it is up it will become a
follower, client will connect to it and use for direct requests. It will use leader only when it is single node cluster.
There is no replication but leader still uses disk to save command to the Raft Log, and it will process this command
during replays which also gives us a good performance improvement.

### Internal processing

Commands and direct calls are processed within same thread on the server side, meaning handling it is completely thread
safe, but there is no guarantee in order processing. The reason is that there are different threads working for
receiving command, replicating it and processing. When command is sent it takes some amount of time to actually process
it. When direct call is made it will skip all of that, process and respond immediately. It means that even if write
command is sent before the direct call, server can still respond with not updated state, even if processing phase of
direct requests and usual requests (commands) are within same thread.

`AdminService.await` will not help either because cluster will respond when majority in the cluster processed command.
It means if there are 3 nodes, leader will process message and respond to the client right after it received
acknowledgment from the first follower and will not wait for the second one at all. When direct call is made to the
follower which was last to respond to leader client will receive older state even when 'await' function was called
before.

Note!
This is normal and expected behavior, with no need to fix it. A read-only request returns the currently available data
with eventual consistency.

Example has retry just to be sure latest state is printed. It is made for demo only and there is no sense at
all to do that in the real application.