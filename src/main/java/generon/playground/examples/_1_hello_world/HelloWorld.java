package generon.playground.examples._1_hello_world;

import generon.client.ClientAPI;
import generon.client.TenantClientAPI;
import generon.client.hello_world_service.HelloWorldServiceAPI;
import generon.client.hello_world_service.HelloWorldServiceAsyncAPI;
import generon.client.hello_world_service.HelloWorldServiceBlockingAPI;
import generon.client.hello_world_service.HelloWorldServiceFactory;
import generon.integration.cluster.ResponseFuture;
import generon.playground.configuration.Example;

public class HelloWorld implements Example {

    @Override
    public void run(ClientAPI clientAPI) {
        TenantClientAPI tenantAPI = clientAPI.tenantAPI(1);

        HelloWorldServiceFactory helloWorldServiceFactory = tenantAPI.helloWorldServiceFactory();

        HelloWorldServiceAPI noReplyApi = helloWorldServiceFactory.noReply();
        HelloWorldServiceAsyncAPI asyncApi = helloWorldServiceFactory.async();
        HelloWorldServiceBlockingAPI blockingApi = helloWorldServiceFactory.blocking();

        noReplyApi.printHello(); // Async without any response. Each node will process and print a message

        ResponseFuture.ObjectFuture<String> helloFuture = asyncApi.hello(); // Async with future object
        asyncApi.hello(response -> System.out.println("1: " + response)); // Async with response

        System.out.println("2: " + helloFuture.join()); // blocking
        System.out.println("3: " + blockingApi.hello()); // blocking
    }

}
