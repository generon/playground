package generon.playground.examples._1_hello_world.common.service;

import generon.core.annotation.scan.service.Service;
import generon.core.annotation.scan.service.ServiceMethod;
import generon.playground.configuration.Constants;

@Service(Constants.Id.Service.HELLO_WORLD)
public interface HelloWorldService {

    @ServiceMethod(1)
    String hello();

    @ServiceMethod(2)
    void printHello();

}
