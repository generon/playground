### Introduction

`HelloWorld` describes how to:

1. Define simple `API` interface
2. Implement this interface on server side to handle incoming requests
3. Use client SDK to send request and receive response

### Interface

`HelloWorldService` has `@Service(N)` which indicates that it is service component. Each service should have own unique
id which is used to route requests. All methods which we want to be accessible from the client should be annotated
with `@ServiceMethod(M)` annotation. ID should be unique within same service. `@Service`and `@ServiceMethod` annotations
have a `version` attribute. It is used to version components and methods. It means we can have two services or methods
with same id but different versions. Interface should be available for both client and server sides. Implementation
class is required for server only. Each `@ServiceMethod` is an API and will be available from SDK to be used. Service
can have methods without annotations. Such methods will not be considered as API methods and will be ignored completely.

### Implementation

`HelloWorldServiceImpl` is an actual request (command) handler. Each request is routed internally to specific service
method by using `service id / version` and `method id / version` combination. Generon will receive request, deserialize
parameters, call service method, serialize return type and send response back to client. Method can be without any
parameter and return void. If return type is `void` client will receive response also.

### Client & SDK

Client connects to the cluster and receives `ClientAPI` object. `ClientAPI` is generated class and has getters
for `GlobalAPI` and `TenantAPI` objects (The difference between these two and the purpose they serve are explained in
the `Multitenancy` example). These classes can give factories for each service, GlobalAPI for global scope and TenantAPI
for tenant scope. For example `HelloWorldServiceFactory factory = tenantAPI.helloWorldServiceFactory()`. Using this
factory we can create 3 types of API objects:

1. **NoReply**. `HelloWorldServiceAPI noReplyApi = helloWorldServiceFactory.noReply()`. This API class sends request
   asynchronously and will not wait response back. Server will know it and not respond at all even if implementation has
   return value. It is useful in some cases and serves optimization purpose.
2. **Async**. `HelloWorldServiceAsyncAPI asyncApi = helloWorldServiceFactory.async()`. This API class sends request
   asynchronously and expects response back. It has two methods per each API interface method and can be used for
   different use-cases:
    1. Method which `returns void` and has `onSuccess and onError callbacks` as last 2 parameters of the method
       signature.
    2. Method which `returns Future object`. `Future` has `join()` method which can be called any time to block current
       thread and get response. Response is returned from `join()` method itself. In case there are some failures it
       will throw an exception, and we can use `try catch` to handle it.
3. **Blocking**. `HelloWorldServiceBlockingAPI blockingApi = helloWorldServiceFactory.blocking()`. This API class sends
   request and blocks current thread until response received. Under the hood it used `async + join` and has same
   behaviour.