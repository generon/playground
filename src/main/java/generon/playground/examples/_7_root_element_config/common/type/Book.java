package generon.playground.examples._7_root_element_config.common.type;

import generon.core.annotation.scan.serializable.Serializable;
import generon.core.annotation.scan.serializable.migration.field.FromVersion;

import java.util.StringJoiner;

@Serializable
public class Book {

    public long id;
    public String name;

    @FromVersion(2)
    public String genre;

    public static Book createV2(long id, String name, String genre) {
        Book book = new Book();
        book.id = id;
        book.name = name;
        book.genre = genre;

        return book;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Book.class.getSimpleName() + "[", "]")
                .add("id=" + id)
                .add("name='" + name + "'")
                .add("genre='" + genre + "'")
                .toString();
    }
}
