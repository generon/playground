package generon.playground.examples._7_root_element_config.server.service;

import generon.core.annotation.Init;
import generon.core.annotation.SnapshotField;
import generon.core.annotation.scan.serializable.migration.object.ObjectVersion;
import generon.core.annotation.scan.serializable.migration.object.StringType;
import generon.core.objects.StringSerialization;
import generon.playground.examples._7_root_element_config.common.service.RootElementConfigService;
import generon.playground.examples._7_root_element_config.common.type.Book;
import generon.playground.examples._7_root_element_config.common.type.ObjectWithRootConfig;
import generon.playground.examples._7_root_element_config.common.type.SimpleEnum;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class RootElementConfigServiceImpl implements RootElementConfigService {

    @SnapshotField(1)
    @ObjectVersion(type = Book.class, value = 2)
    @StringType(StringSerialization.ASCII)
    private final Map<String, List<Book>> usebooksByGenre = new HashMap<>();

    @Init
    private void init() {
        if (!usebooksByGenre.isEmpty()) {
            return;
        }

        String fantasyGenre = "Fantasy";
        String scienceGenre = "Science";

        List<Book> fantasyBooks = new ArrayList<>();
        List<Book> scienceBooks = new ArrayList<>();

        fantasyBooks.add(Book.createV2(1, "Book1", fantasyGenre));
        fantasyBooks.add(Book.createV2(2, "Book2", fantasyGenre));

        scienceBooks.add(Book.createV2(3, "Book3", scienceGenre));

        usebooksByGenre.put(fantasyGenre, fantasyBooks);
        usebooksByGenre.put(scienceGenre, scienceBooks);
    }

    @Override
    public String string(String asciiString) {
        return asciiString;
    }

    @Override
    public SimpleEnum enumType(SimpleEnum ordinalEnum) {
        return ordinalEnum;
    }

    @Override
    public ObjectWithRootConfig objectWithRootConfigV1(ObjectWithRootConfig object) {
        return object;
    }

    @Override
    public ObjectWithRootConfig objectWithRootConfigV2(ObjectWithRootConfig object) {
        return object;
    }

    @Override
    public Map<String, List<Book>> findBooksByGenre(Set<String> genres) {
        if (genres == null) {
            return usebooksByGenre;
        }

        if (genres.isEmpty()) {
            return Collections.emptyMap();
        }

        Map<String, List<Book>> result = new HashMap<>();

        for (String genre : genres) {
            List<Book> books = usebooksByGenre.get(genre);

            if (books != null && !books.isEmpty()) {
                result.put(genre, books);
            }
        }

        return result;
    }
}
