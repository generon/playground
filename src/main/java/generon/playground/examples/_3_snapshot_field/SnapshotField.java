package generon.playground.examples._3_snapshot_field;

import generon.client.ClientAPI;
import generon.client.GlobalClientAPI;
import generon.client.TenantClientAPI;
import generon.client.admin_service.AdminServiceBlockingAPI;
import generon.client.snapshot_field_service.SnapshotFieldServiceBlockingAPI;
import generon.playground.configuration.ConsoleColor;
import generon.playground.configuration.TestRunner;
import generon.playground.configuration.TestRunnerContext;
import org.agrona.collections.IntArrayList;

public class SnapshotField {

    private static final String TAG = SnapshotField.class.getSimpleName();

    public static void run(ClientAPI clientAPI, boolean takeSnapshot) {
        GlobalClientAPI globalAPI = clientAPI.globalAPI();
        TenantClientAPI tenantAPI = clientAPI.tenantAPI(1);

        AdminServiceBlockingAPI adminServiceAPI = globalAPI.adminServiceFactory().blocking();
        SnapshotFieldServiceBlockingAPI snapshotFieldAPI = tenantAPI.snapshotFieldServiceFactory().blocking();

        IntArrayList newNumberState = snapshotFieldAPI.addNumbers(new int[]{1, 2});
        System.out.println("State: " + newNumberState);

        if (takeSnapshot) {
            adminServiceAPI.takeSnapshot();
            adminServiceAPI.await();
        }
    }

    public static void run() {
        String run1 = "Run 1. Add some initial data";
        String run2 = "Run 2. Reply state from the 'Run 1', add more data and do a snapshot at the end";
        String run3 = "Run 3. Apply snapshot from 'Run 2', add more data";

        TestRunner.startTag(TAG);
        TestRunner.separateLines(
                () -> run(false, run1),
                () -> run(true, run2),
                () -> run(false, run3)
        );
        TestRunner.endTag(TAG);

        TestRunner.deleteAllData(); // Make sure this example does not affect next ones
    }

    public static void run(boolean takeSnapshot, String description) {
        ConsoleColor.WHITE_BOLD.print(description);

        TestRunnerContext context = new TestRunnerContext()
                .consumer(clientAPI -> run(clientAPI, takeSnapshot))
                .diskComponentCleanup(false);

        TestRunner.run(context);
    }

}
