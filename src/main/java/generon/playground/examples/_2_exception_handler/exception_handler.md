### General overview

Generon provides a way for the client to handle exceptions which may happen during the request or command processing. It
can be timeouts, validation or any other exceptions. There is a need for the client to easily handle such cases and make
decisions based on exception.

### How to handle exceptions ?

Client can detect it when API request is made:

1. Using `onError` callback from the `asyncApi.request(parameters, Consumer onSuccess, Consumer<ErrorMessage> onError)`.
   When exception happened `onError` callback will be executed instead of `onSuccess`. ErrorMessage contains `int code`
   and `String message`.
2. Using blockingApi call (or async + join). To handle the exception we need to use usual `try/catch` block.

### Error codes

`GeneronProcessingException` has `int code` and `String message` parameters. When async request is made code and message
can be taken from `ErrorMessage` object. When there is blocking request (or async + join) it can be taken from
caught `GeneronProcessingException` exception. It is a good practice to have `unique code per each exception` for the
whole backend which allow client to make own decision based on this code and not rely on the message text itself. When
any other exception is thrown code will have default value of `-1`.

System codes are negative and listed as constants inside of `ErrorMessage` class:

1. `UNKNOWN_CODE (-1)` - Used for any exception except `GeneronProcessingException`.
2. `TIMEOUT_CODE (-2)` - Used for timeouts when response is not received for some period of time. Default timeout
   is `5000ms`. Use `GeneronClientContext.timeoutContext(...)` to change it. Client will get `GeneronTimeoutException`.
3. `UNKNOWN_VERSION_CODE (-3)` - Used for cases when received wrong object version which is not expected by the
   configuration. It is `additional safety` mechanism which will `ensure data compatibility` among all sides (client &
   server). Possible expected scenario is when only server or client code is updated and deployed without another side,
   in this cases data configuration can have differences.

### Error Logging

We can configure additional `error handler and log strategy` for `GeneronClientContext` or `GeneronNodeContext` using
method `.errorContext(errorHandler, logStrategy)`:

1. `ErrorHandler` is a `consumer` with `Throwable`. By default, it just runs printStackTrace method.
2. `ErrorLogStrategy` is used in addition to errors logging logic. It has 3 values:
    - `ALWAYS`. Default one. Will log all errors and call `ErrorHandler` each time if not null
    - `IF_NO_HANDLER`. Will log only when `ErrorHandler` is null
    - `NEVER`. Will not log.

### Example

`ExceptionHandlerService` has 1 method which takes `int action` as parameter. Based on this parameter server will throw
some exceptions or sleep to trigger timeout on the client side.

1. When action is 1 server will throw `GeneronProcessingException` with message and specified code.
2. When action is 2 server will throw `IllegalStateException` (Or it can be any other exception). `Error code` will
   be `-1`. Client will get this exception as `GeneronException`.
3. When action is 3 server will sleep for some time to trigger timeout on the client side. Client will get this
   exception as `GeneronTimeoutException`.