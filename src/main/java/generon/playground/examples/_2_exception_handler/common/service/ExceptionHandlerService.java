package generon.playground.examples._2_exception_handler.common.service;

import generon.core.annotation.scan.service.Service;
import generon.core.annotation.scan.service.ServiceMethod;
import generon.playground.configuration.Constants;

@Service(Constants.Id.Service.EXCEPTION_HANDLER)
public interface ExceptionHandlerService {

    @ServiceMethod(1)
    String testException(int actionId);

}
