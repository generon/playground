package generon.playground.examples._12_security.server.service;

import generon.core.annotation.Inject;
import generon.integration.cluster.server.CommandContext;
import generon.integration.objects.GeneronProcessingException;
import generon.playground.examples._12_security.common.event_producer.SecurityEventProducer;
import generon.playground.examples._12_security.common.service.SecurityService;

public class SecurityServiceImpl implements SecurityService {

    private static final int SECURITY_CODE = 1;
    private static final byte PERMISSION_MASK = 0b00000001;

    @Inject
    private CommandContext commandContext;

    @Inject
    private SecurityEventProducer securityEventProducer;

    @Override
    public String securedMethod() {
        if (!hasPermission(PERMISSION_MASK)) {
            throw new GeneronProcessingException(SECURITY_CODE, "Permission denied");
        }

        return "Has permission"; // Code should not reach here
    }

    @Override
    public void triggerEvent() {
        securityEventProducer.event();
    }

    private boolean hasPermission(byte permissionMask) {
        byte[] principal = commandContext.session().encodedPrincipal();

        if (principal.length != 1) {
            return false;
        }

        return (principal[0] & permissionMask) == 1; // Verify permission bit
    }
}
