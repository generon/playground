package generon.playground.examples._12_security.server.configuration.security;

import generon.integration.cluster.server.security.GeneronAuthenticatorHooks;
import generon.integration.cluster.server.security.SessionState;

public class AuthenticatorHooksImpl implements GeneronAuthenticatorHooks {

    @Override
    public void onConnectRequest(long sessionId, long timestamp, SessionState sessionState) {
        String messageBase = switch (sessionState.status()) {
            case CHALLENGE -> "Client is connected. Switching to 'challenge' state";
            case REJECT -> "Client is not connected. Switching to 'reject' state";
            default -> throw new IllegalStateException("Unknown session state: " + sessionState);
        };

        System.out.println(messageBase + ". Login: '" + sessionState.login() + "', SessionId: " + sessionId + ", Time: " + timestamp);
    }

    @Override
    public void onConnectedSession(long sessionId, long timestamp, SessionState sessionState) {
        String messageBase = switch (sessionState.status()) {
            case CHALLENGE -> "Challenge is requested";
            case REJECT -> "Session is rejected";
            default -> throw new IllegalStateException("Unknown session state: " + sessionState);
        };

        System.out.println(messageBase + ". SessionId: " + sessionId + ", Time: " + timestamp);
    }

    @Override
    public void onChallengeResponse(long sessionId, long timestamp, SessionState sessionState) {
        String messageBase = switch (sessionState.status()) {
            case AUTHENTICATED -> "Challenge is successful. Switching to 'authenticated' state";
            case REJECT -> "Challenge is failed. Switching to 'reject' state";
            default -> throw new IllegalStateException("Unknown session state: " + sessionState);
        };

        System.out.println(messageBase + ". SessionId: " + sessionId + ", Time: " + timestamp);
    }

    @Override
    public void onChallengedSession(long sessionId, long timestamp, SessionState sessionState) {
        String messageBase = switch (sessionState.status()) {
            case AUTHENTICATED -> "Session is authenticated";
            case REJECT -> "Session is rejected";
            default -> throw new IllegalStateException("Unknown session state: " + sessionState);
        };

        System.out.println(messageBase + ". SessionId: " + sessionId + ", Time: " + timestamp);
    }
}
