package generon.playground.examples._12_security.server.configuration.security;

import generon.core.util.ByteUtils;
import generon.integration.cluster.server.security.GeneronServerChallengeService;

import java.util.Random;

public class ServerChallengeServiceImpl extends GeneronServerChallengeService {

    private static final Random random = new Random();

    @Override
    protected byte[] request(long sessionId) {
        return ByteUtils.fromInt(random.nextInt(0, 100_000));
    }

    @Override
    protected boolean validateResponse(long sessionId, byte[] request, byte[] response) {
        if (response == null || response.length != 4) {
            return false;
        }

        return ByteUtils.toInt(request) + 1 == ByteUtils.toInt(response);
    }
}
