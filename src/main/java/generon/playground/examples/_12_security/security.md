### General overview

Each product needs protection from various angles. This can include IP whitelisting, firewalls, SSL/TLS, and much more.
The first layer is usually application side security, which is focused on authentication and authorization checks.
Simple example is a login/password flow. Server should store user's data with encoded password. To encode password we
need to use some kind of security key - usually called `pepper`. During authentication process we will `match` encoded
password with given password and verify connection. This process can also have additional steps of protection. After
password is validated we can ask client to solve a `challenge` during which client should transform input data in a way
that server will accept it. For example, we can have a rule that server will send a random number and client should
respond with `number + 1` all the time. All parts of our system should be aware of this contract to be authenticated.
Also, `+ 1` part could also be dynamic and be based on current time like calendar day, similar to how enigma machine did
it. This formula or contract should not be shared through the network. Using double authentication process we are adding
more protection to the system. Additionally, communication itself should be encrypted too. It will ensure that data like
passwords will not go via network as is. In our case we have a cluster as a core and client is not end user, usually it
is a gateway or another microservice from our system and security layer is adjusted for this case.

There are two types of connections:

1. Cluster connection. When client connects to the cluster to call some APIs. Direct connection is based on the cluster
   connection and does not need additional verifications.
2. Archive connection. When client connects to one or multiple nodes to consumer events. Archive is a separate component
   of Aeron ecosystem, meaning each node (or application) should also be authenticated to its own archive in a same way
   as client does.

Both these connection types use the same authentication flow and can be customized if needed.

Connection will have next steps:

1. Client initializes connection to the server, sends login and password which server should recognize. Request will
   be encoded via security key known by client and server.
2. Server receives the request and decodes it via same security key. It takes encoded password from the storage by login
   and matches it with client`s input and password security key. If server does not know received login or password is
   incorrect connection will be rejected. If all good, it will proceed to the next step.
3. Second step is a `challenge`. This step is a dynamic approach to verify client, but at the same time we will not send
   any keys via network. For example for any number server sends, client should add 1 and respond with new number. With
   this approach server can send random numbers and for attacker it will mean nothing because formula sits locally on
   both sides. For example server sends number 10 to the client, and expects 11 as correct answer.
4. Client receives this number, which is 10 in our case and knowing the contract it sends 11 as a response.
5. Server receives a response and knowing request which it sent and contract itself which is `N + 1` it will
   authenticate connection fully. If for any reason response is not 11 connection will be rejected.

- Client and server will have a key used for encoding/decoding requests.
- Additionally, server has one more key used for passwords encoding to store them in a safe manner.

### Generon classes

#### Server

1. `GeneronServerCredentialSupplier`. Interface that returns not encoded login/password pairs.
2. `GeneronServerCredentialService`. Login/Password storage. Has `pepper` value which is used to encode passwords.
   It uses `GeneronServerCredentialSupplier` to initialize state.
3. `GeneronServerChallengeService`. Used to send challenge request to the client and validate response.
4. `GeneronServerPrincipalService`. Builds a `byte[]` representation of principal, takes `login` and `data` fields and
   returns `byte[]`. This `byte[]` can be injected and used to verify some permissions or identify sessions.
5. `GeneronAuthenticatorHooks`. Used to intercept authentication logic. By default, empty implementation is used.
6. `GeneronAuthenticator`. Main authentication class which combines all these components together.

#### Client

1. `GeneronClientCredentialsSupplier`. Contains `login`, `password` and abstract `challenge` handler.

### Example classes

#### Server

1. `ServerCredentialSupplierImpl`. The first and second examples use lambda
   with `Map.of("my_gateway", "my_gateway_password")`. The third example uses `EnvironmentCredentialsSupplier` which
   will read `generon_credentials` variable to get credentials, and it has next
   structure - `-Dgeneron_credentials=login1:pass1,login2:pass2`.
2. `ServerChallengeServiceImpl` extends `GeneronServerChallengeService`.
   Implements `request` and `validateResponse` methods. Request sends random int value. Validation checks that value is
   incremented by 1.
3. `ServerPrincipalServiceImpl` has simple implementation for initializing principal object on the server side. It sets
   `byte[]` with single byte which represents bitset of permissions. Example has 1 API which is checking that principal
   has permission bit enabled. If it does not have required permission server will throw and exception.
4. `AuthenticatorHooksImpl` implements `GeneronAuthenticatorHooks`.
   Used to intercept and log server side authentication process.

#### Client

1. `ClientCredentialsSupplierImpl` extends `GeneronClientCredentialsSupplier`.
   Used to set client credentials and implement challenge handler. Login is `my_gateway`, password
   is `my_gateway_password`. Challenge formula is `N + 1`, which adds 1 to all input numbers.

### Principal

When client is authenticated server side can use its data and create `byte[]` principal which will be attached to the
session. This principal can be used during requests to verify permissions by injecting `CommandContext` and
using `byte[] principal = commandContext.session().encodedPrincipal()` to get principal. `CommandContext` is updated
on each request and always has current request (command) and client session information.