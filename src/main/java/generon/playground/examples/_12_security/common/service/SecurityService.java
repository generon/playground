package generon.playground.examples._12_security.common.service;

import generon.core.annotation.scan.service.Service;
import generon.core.annotation.scan.service.ServiceMethod;
import generon.playground.configuration.Constants;

@Service(Constants.Id.Service.SECURITY)
public interface SecurityService {

    @ServiceMethod(1)
    String securedMethod();

    @ServiceMethod(2)
    void triggerEvent();

}
