package generon.playground.examples._12_security.common.component;

import generon.core.annotation.Inject;
import generon.integration.cluster.server.CommandContext;

import java.util.Arrays;

public class SecurityComponent {

    @Inject
    private CommandContext commandContext;

    public void securedMethod1() {
        if (!hasPermission(1)) {
            throw new SecurityException("You do not have permission to access this resource");
        }

        System.out.println("working");
    }

    private boolean hasPermission(int data) {
        byte[] principal = commandContext.session().encodedPrincipal();
        System.out.println(Arrays.toString(principal));
        return true;
    }
}