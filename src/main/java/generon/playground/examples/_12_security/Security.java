package generon.playground.examples._12_security;

import generon.client.EventConsumerContext;
import generon.client.EventManager;
import generon.client.TenantClientAPI;
import generon.client.security_service.SecurityServiceBlockingAPI;
import generon.client.security_service.SecurityServiceFactory;
import generon.integration.cluster.server.security.EnvironmentCredentialsSupplier;
import generon.integration.cluster.server.security.GeneronAuthenticator;
import generon.integration.cluster.server.security.GeneronAuthenticatorHooks;
import generon.integration.cluster.server.security.GeneronServerChallengeService;
import generon.integration.cluster.server.security.GeneronServerCredentialService;
import generon.integration.cluster.server.security.GeneronServerCredentialSupplier;
import generon.integration.cluster.server.security.GeneronServerPrincipalService;
import generon.integration.objects.ErrorLogStrategy;
import generon.integration.objects.GeneronProcessingException;
import generon.playground.configuration.ConsoleColor;
import generon.playground.configuration.TestConfigurator;
import generon.playground.configuration.TestRunner;
import generon.playground.configuration.TestRunnerContext;
import generon.playground.examples._12_security.client.configuration.security.ClientCredentialsSupplierImpl;
import generon.playground.examples._12_security.client.event_listener.SecurityEventListenerImpl;
import generon.playground.examples._12_security.server.configuration.security.AuthenticatorHooksImpl;
import generon.playground.examples._12_security.server.configuration.security.ServerChallengeServiceImpl;
import generon.playground.examples._12_security.server.configuration.security.ServerPrincipalServiceImpl;
import io.aeron.security.AuthenticationException;
import io.aeron.security.CredentialsSupplier;
import org.agrona.ErrorHandler;

import java.util.Map;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

public class Security {

    public static final String LOGIN = "my_gateway";
    public static final String PASSWORD = "my_gateway_password";
    public static final String DATA = "Additional data";

    public static final String COMMUNICATION_SECURITY_KEY = "communication_key_123";
    public static final String PASSWORD_SECURITY_KEY = "pepper_123";

    private static final String TAG = Security.class.getSimpleName();

    public static void run() {
        String run1 = "Run 1. Cluster connection. Use correct password";
        String run2 = "Run 2. Cluster connection. Use wrong password";
        String run3 = "Run 3. Cluster connection. Use environment credential provider (Need to set variable)";
        String run4 = "Run 4. Archive connection. Use correct password (Has the same flow and behavior)";
        String run5 = "Run 5. Principal permission verification. Validate and throw exception";

        TestRunner.startTag(TAG);
        TestRunner.separateLines(
                () -> clusterConnectionRun(correctCredentialsConfig(), run1),
                () -> clusterConnectionRun(wrongCredentialsConfig(), run2),
                () -> clusterConnectionRun(environmentCredentialsConfig(), run3),
                () -> archiveConnectionRun(run4),
                () -> principalPermissionRun(run5)
        );
        TestRunner.endTag(TAG);
    }

    private static void clusterConnectionRun(TestConfigurator configurator, String description) {
        ConsoleColor.WHITE_BOLD.print(description);

        TestRunnerContext context = new TestRunnerContext()
                .configurator(configurator)
                .consumer(null); // This example demonstrates only connection part

        TestRunner.run(context);
    }

    private static void archiveConnectionRun(String description) {
        ConsoleColor.WHITE_BOLD.print(description);

        TestConfigurator testConfigurator = archiveConnectionConfigurator();

        TestRunnerContext context = new TestRunnerContext()
                .configurator(testConfigurator)
                .consumer(clientAPI -> {
                    TenantClientAPI tenantAPI = clientAPI.tenantAPI(1);

                    SecurityServiceFactory factory = tenantAPI.securityServiceFactory();
                    SecurityServiceBlockingAPI api = factory.blocking();

                    api.triggerEvent();

                    // Connect to the archive of some node and handle event
                    int consumerId = 1;
                    CountDownLatch eventLatch = new CountDownLatch(1);

                    EventConsumerContext eventConsumerContext = new EventConsumerContext.Builder(consumerId)
                            .addListener(new SecurityEventListenerImpl(eventLatch))
                            .build();

                    EventManager eventManager = clientAPI.eventManager();
                    eventManager.connectConsumer(eventConsumerContext);

                    if (!eventLatch.await(5, TimeUnit.SECONDS)) {
                        System.err.println("Event timeout");
                    }

                    eventManager.closeConsumer(consumerId);
                });

        TestRunner.run(context);
    }

    private static void principalPermissionRun(String description) {
        ConsoleColor.WHITE_BOLD.print(description);

        TestConfigurator testConfigurator = noPrincipalPermissionConfigurator();

        TestRunnerContext context = new TestRunnerContext()
                .configurator(testConfigurator)
                .consumer(clientAPI -> {
                    TenantClientAPI tenantAPI = clientAPI.tenantAPI(1);

                    SecurityServiceFactory factory = tenantAPI.securityServiceFactory();
                    SecurityServiceBlockingAPI api = factory.blocking();

                    try {
                        api.securedMethod();
                    } catch (GeneronProcessingException e) {
                        System.out.println(e.toErrorMessage());
                    }
                });

        TestRunner.run(context);
    }

    private static TestConfigurator correctCredentialsConfig() {
        return clusterConnectionConfigurator(() -> Map.of(LOGIN, PASSWORD), correctClientCredentialSupplier(), null);
    }

    private static TestConfigurator wrongCredentialsConfig() {
        return clusterConnectionConfigurator(
                () -> Map.of(LOGIN, PASSWORD),
                wrongClientCredentialSupplier(),
                t -> {
                    if (t instanceof AuthenticationException) {
                        System.out.println("Client is not connected. Connection is rejected by server side due to invalid password");
                    }
                }
        );
    }

    private static TestConfigurator environmentCredentialsConfig() {
        return clusterConnectionConfigurator(
                new EnvironmentCredentialsSupplier(),
                correctClientCredentialSupplier(),
                t -> {
                    if (t instanceof AuthenticationException) {
                        String parameter = String.format("'-Dgeneron_credentials=%s:%s'", LOGIN, PASSWORD);
                        System.out.println("Client is not connected. Set environment variable as " + parameter + " to ensure the server identifies the client");
                    }
                }
        );
    }

    private static TestConfigurator clusterConnectionConfigurator(
            GeneronServerCredentialSupplier serverCredentialSupplier,
            CredentialsSupplier clientCredentialsSupplier,
            ErrorHandler errorHandler
    ) {
        return new TestConfigurator(
                nc -> nc.consensusModule(c -> c.authenticatorSupplier(() -> {
                    GeneronServerCredentialService serverCredentialService = new GeneronServerCredentialService(PASSWORD_SECURITY_KEY, serverCredentialSupplier);
                    GeneronServerChallengeService serverChallengeService = new ServerChallengeServiceImpl();
                    GeneronAuthenticatorHooks authenticatorHooks = new AuthenticatorHooksImpl();

                    return new GeneronAuthenticator(
                            serverCredentialService, serverChallengeService, GeneronServerPrincipalService.DEFAULT,
                            authenticatorHooks, COMMUNICATION_SECURITY_KEY
                    );
                })),
                cc -> {
                    cc.aeronCluster(ac -> ac.credentialsSupplier(clientCredentialsSupplier));
                    cc.errorContext(
                            errorHandler == null ? cc.errorContext().errorHandler() : errorHandler,
                            ErrorLogStrategy.NEVER
                    );
                }
        );
    }

    private static TestConfigurator archiveConnectionConfigurator() {
        GeneronServerCredentialSupplier serverCredentialSupplier = () -> Map.of(LOGIN, PASSWORD);
        CredentialsSupplier clientCredentialsSupplier = correctClientCredentialSupplier();

        return new TestConfigurator(
                nc -> {
                    nc.archive(a -> a.authenticatorSupplier(() -> {
                        GeneronServerCredentialService serverCredentialService = new GeneronServerCredentialService(PASSWORD_SECURITY_KEY, serverCredentialSupplier);
                        GeneronServerChallengeService serverChallengeService = new ServerChallengeServiceImpl();

                        return new GeneronAuthenticator(
                                serverCredentialService,
                                serverChallengeService,
                                GeneronServerPrincipalService.DEFAULT,
                                GeneronAuthenticatorHooks.DEFAULT,
                                COMMUNICATION_SECURITY_KEY
                        );
                    }));
                    nc.consensusModule(c -> c.archiveContext().credentialsSupplier(clientCredentialsSupplier));
                    nc.aeronArchive(a -> a.credentialsSupplier(clientCredentialsSupplier));
                    nc.clusteredServiceContainerCustomizer(c -> c.archiveContext().credentialsSupplier(clientCredentialsSupplier));
                },
                cc -> cc.aeronArchiveCustomizer((context, __) -> context.credentialsSupplier(clientCredentialsSupplier))
        );
    }

    private static TestConfigurator noPrincipalPermissionConfigurator() {
        return new TestConfigurator(
                nc -> {
                    nc.consensusModule(c -> c.authenticatorSupplier(() -> {
                        GeneronServerCredentialSupplier serverCredentialSupplier = () -> Map.of(LOGIN, PASSWORD);
                        GeneronServerCredentialService serverCredentialService = new GeneronServerCredentialService(PASSWORD_SECURITY_KEY, serverCredentialSupplier);
                        GeneronServerChallengeService serverChallengeService = new ServerChallengeServiceImpl();
                        GeneronServerPrincipalService serverPrincipalService = new ServerPrincipalServiceImpl(Map.of(LOGIN, (byte) 0b0000000));

                        return new GeneronAuthenticator(
                                serverCredentialService,
                                serverChallengeService,
                                serverPrincipalService,
                                AuthenticatorHooksImpl.DEFAULT,
                                COMMUNICATION_SECURITY_KEY
                        );
                    }));
                    nc.errorContext(null, ErrorLogStrategy.NEVER); // Disable expected error logs
                },
                cc -> {
                    cc.aeronCluster(ac -> ac.credentialsSupplier(correctClientCredentialSupplier()));
                    cc.errorContext(null, ErrorLogStrategy.NEVER); // Disable expected error logs
                }
        );
    }

    private static ClientCredentialsSupplierImpl correctClientCredentialSupplier() {
        return new ClientCredentialsSupplierImpl(LOGIN, PASSWORD, DATA, COMMUNICATION_SECURITY_KEY);
    }

    private static ClientCredentialsSupplierImpl wrongClientCredentialSupplier() {
        return new ClientCredentialsSupplierImpl(LOGIN, "wrong_password", DATA, COMMUNICATION_SECURITY_KEY);
    }
}
