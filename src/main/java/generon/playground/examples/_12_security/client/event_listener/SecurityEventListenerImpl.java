package generon.playground.examples._12_security.client.event_listener;

import generon.client.security_event_producer.SecurityEventListener;
import generon.integration.cluster.objects.header.EventHeader;

import java.util.concurrent.CountDownLatch;

public class SecurityEventListenerImpl implements SecurityEventListener {

    private final CountDownLatch countDownLatch;

    public SecurityEventListenerImpl(CountDownLatch countDownLatch) {
        this.countDownLatch = countDownLatch;
    }

    @Override
    public void onEvent(EventHeader header) {
        System.out.println("Event is received");
        countDownLatch.countDown();
    }

}
