package generon.playground.examples._12_security.client.configuration.security;

import generon.core.util.ByteUtils;
import generon.integration.cluster.client.security.GeneronClientCredentialsSupplier;

public class ClientCredentialsSupplierImpl extends GeneronClientCredentialsSupplier {

    public ClientCredentialsSupplierImpl(String login, String password, String data, String communicationSecurityKey) {
        super(login, password, data, communicationSecurityKey);
    }

    @Override
    public byte[] onChallenge(byte[] encodedChallenge) {
        int request = ByteUtils.toInt(encodedChallenge);
        return ByteUtils.fromInt(request + 1);
    }

}
