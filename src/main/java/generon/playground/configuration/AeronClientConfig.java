package generon.playground.configuration;

import generon.integration.cluster.client.AeronClientProperties;
import generon.integration.cluster.server.GeneronClusterMember;
import generon.integration.cluster.server.GeneronNodeClientContext;

import java.util.List;

public class AeronClientConfig {

    public static final int EGRESS_PORT = 11000;
    public static final int EVENT_SUBSCRIPTION_PORT = 11001;
    public static final int DIRECT_SUBSCRIPTION_PORT = 11002;
    public static final int DIRECT_SUBSCRIPTION_STREAM_ID = 1;

    public static AeronClientProperties of(List<GeneronClusterMember> clusterMembers) {
        return AeronClientProperties.of(
                "localhost", clusterMembers,
                EGRESS_PORT, EVENT_SUBSCRIPTION_PORT,
                DIRECT_SUBSCRIPTION_PORT, DIRECT_SUBSCRIPTION_STREAM_ID
        );
    }

    public static GeneronNodeClientContext toNodeClientContext() {
        return new GeneronNodeClientContext(DIRECT_SUBSCRIPTION_PORT, DIRECT_SUBSCRIPTION_STREAM_ID);
    }

}