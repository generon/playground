package generon.playground.configuration;

public class Constants {

    public static final class Id {

        public static final class Component {
            public static final short ENTITY_ID_COUNTER = 1;
            public static final short TIMER_MANAGER = 2;
        }

        public static final class Configuration {
        }

        public static final class Service {
            public static final short ADMIN = -1;

            public static final short HELLO_WORLD = 1;
            public static final short EXCEPTION_HANDLER = 2;
            public static final short SNAPSHOT_FIELD = 3;
            public static final short TYPE_SUPPORT = 4;
            public static final short CUSTOM_TYPE = 5;
            public static final short SERIALIZABLE_OBJECT = 6;
            public static final short ROOT_ELEMENT_CONFIG = 7;
            public static final short DEPENDENCY_INJECTION = 8;
            public static final short PROPERTY_MANAGEMENT = 9;
            public static final short EVENT_MANAGEMENT = 10;
            public static final short TIMER_MANAGEMENT = 11;
            public static final short SECURITY = 12;
            public static final short MIGRATION_MANAGEMENT = 13;
            public static final short DIRECT_CALL = 14;
            public static final short GLOBAL_SCOPE = 15;
            public static final short TENANT_SCOPE = 16;
        }

        public static final class EventProducer {
            public static final short PRODUCT = 1;
            public static final short SECURITY = 2;
            public static final short MIGRATION = 3;
        }

    }

    public static final class EventStreamId {
        public static final int MIGRATION = 1;

        public static final class Product {
            public static final int CRUD = 1001;
            public static final int STOCK = 1002;
            public static final int DISCOUNT = 1003;
        }
    }

}
