package generon.playground.configuration;

import generon.client.ClientAPI;

@FunctionalInterface
public interface Example {

    void run(ClientAPI clientAPI) throws Exception;

}
