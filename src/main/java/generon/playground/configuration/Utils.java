package generon.playground.configuration;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Utils {

    public static void sleep(long millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    public static String deepToString(Object obj) {
        if (obj == null) {
            return "null";
        }

        if (obj.getClass().isArray()) {
            if (obj.getClass().getComponentType().isArray()) {
                int length = Array.getLength(obj);
                List<String> list = new ArrayList<>(length);

                for (int i = 0; i < length; i++) {
                    list.add(deepToString(Array.get(obj, i)));
                }

                return list.toString();
            }

            return Arrays.toString((Object[]) obj);
        }

        if (obj instanceof Collection<?> collection) {
            return collection.stream()
                    .map(Utils::deepToString)
                    .toList()
                    .toString();
        }

        if (obj instanceof Map<?, ?> map) {
            return map.entrySet().stream()
                    .map(entry -> deepToString(entry.getKey()) + "=" + deepToString(entry.getValue()))
                    .collect(Collectors.joining(", ", "{", "}"));
        }

        return obj.toString();
    }

}
