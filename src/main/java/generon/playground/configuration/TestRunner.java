package generon.playground.configuration;

import generon.client.ClientAPI;
import generon.core.util.lambda.ExceptionConsumer;
import generon.integration.cluster.client.AeronClientProperties;
import generon.integration.cluster.client.GeneronClient;
import generon.integration.cluster.client.GeneronClientContext;
import generon.integration.cluster.server.GeneronClusterMember;
import generon.integration.cluster.server.GeneronNode;
import generon.integration.cluster.server.GeneronNodeClientContext;
import generon.integration.cluster.server.GeneronNodeContext;
import generon.integration.util.lambda.ExceptionRunnable;
import io.aeron.cluster.service.Cluster;
import org.agrona.CloseHelper;

import java.io.File;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class TestRunner {

    /**
     * Amount of nodes in the cluster, should be an odd number
     */
    private static final int NODE_SIZE = 3;

    private static final String DIRECTORY_PREFIX = "aeron_directory";

    static {
        if (NODE_SIZE % 2 == 0) {
            throw new IllegalCallerException("NODE_SIZE should always be an odd number");
        }
    }

    public static ExceptionRunnable runnable(ConfigurableExample configurableExample) {
        return () -> run(configurableExample);
    }

    public static ExceptionRunnable runnable(TestRunnerContext context) {
        return () -> run(context);
    }

    public static void run(ConfigurableExample configurableExample) {
        TestRunnerContext configurator = new TestRunnerContext()
                .consumer(configurableExample::run)
                .configurator(configurableExample.testConfigurator());

        run(configurator);
    }

    public static void run(TestRunnerContext context) {
        ExceptionConsumer<ClientAPI> consumer = context.consumer();
        TestConfigurator testConfigurator = context.configurator();
        boolean enableComponentCleanup = context.diskComponentCleanup();

        List<GeneronNode> generonNodeList = new ArrayList<>(NODE_SIZE);

        Map<Cluster.Role, List<GeneronNode>> nodeByRole = null;
        GeneronClient client = null;

        try {
            // Prepare cluster members and client context
            List<GeneronClusterMember> clusterMembers = IntStream.range(0, NODE_SIZE)
                    .mapToObj(i -> GeneronClusterMember.create(i, "localhost"))
                    .toList();

            GeneronNodeClientContext nodeClientContext = AeronClientConfig.toNodeClientContext();

            // Start cluster and client
            clusterMembers.stream().map(clusterMember -> {
                int memberId = clusterMember.id();
                GeneronNodeContext generonNodeContext = new GeneronNodeContext(memberId, clusterMembers, DIRECTORY_PREFIX + "/node" + memberId, nodeClientContext);

                if (enableComponentCleanup) {
                    generonNodeContext.enableComponentCleanup();
                }

                testConfigurator.configure(generonNodeContext);
                GeneronNode generonNode = GeneronNode.launch(generonNodeContext);

                while (!generonNode.clusteredService().isStarted()) {
                    Utils.sleep(1); // Sequential start
                }

                return generonNode;
            }).forEach(generonNodeList::add);

            while (generonNodeList.stream().anyMatch(an -> an.clusteredService().cluster() == null)) {
                Utils.sleep(1);
            }

            while (generonNodeList.stream().noneMatch(an -> an.clusteredService().cluster().role() == Cluster.Role.LEADER)) {
                Utils.sleep(1);
            }

            nodeByRole = generonNodeList.stream()
                    .collect(Collectors.groupingBy(an -> an.clusteredService().cluster().role()));

            ConsoleColor.YELLOW_BOLD.print("---------- Start. " + memberRoles(nodeByRole) + " ----------");

            // Prepare and start client
            int requestThreadPoolSize = 2;
            int eventConsumerThreadPoolSize = 1;

            String clientAeronDirectory = DIRECTORY_PREFIX + "/client";

            AeronClientProperties aeronClientProperties = AeronClientConfig.of(clusterMembers);
            GeneronClientContext generonClientContext = new GeneronClientContext(clusterMembers, requestThreadPoolSize, eventConsumerThreadPoolSize, clientAeronDirectory, aeronClientProperties);

            testConfigurator.configure(generonClientContext);
            client = GeneronClient.launch(generonClientContext);

            if (client != null && consumer != null) {
                ClientAPI clientAPI = client.api(ClientAPI::new);

                consumer.accept(clientAPI);
                clientAPI.globalAPI().adminServiceFactory().blocking().await(); // Await async operations
            }
        } catch (Throwable e) {
            e.printStackTrace();
        } finally {
            CloseHelper.quietClose(client);

            if (nodeByRole != null) {
                CloseHelper.quietCloseAll(nodeByRole.get(Cluster.Role.CANDIDATE));
                CloseHelper.quietCloseAll(nodeByRole.get(Cluster.Role.FOLLOWER));
                CloseHelper.quietCloseAll(nodeByRole.get(Cluster.Role.LEADER));
            } else {
                CloseHelper.quietCloseAll(generonNodeList);
            }

            ConsoleColor.YELLOW_BOLD.print("---------- Finish ----------");
        }
    }

    public static void runPipeline(ExceptionRunnable... executions) {
        deleteAllData();

        if (executions == null || executions.length == 0) {
            return;
        }

        for (int i = 0; i < executions.length; i++) {
            ExceptionRunnable execution = executions[i];

            if (execution == null) {
                continue;
            }

            try {
                execution.run();
            } catch (Exception e) {
                throw new RuntimeException(e);
            } finally {
                deleteAllData();
            }

            if (i != executions.length - 1) {
                System.out.println();
            }
        }
    }

    public static ExceptionRunnable group(Example... examples) {
        if (examples == null || examples.length == 0) {
            return null;
        }

        return () -> {
            ExceptionConsumer<ClientAPI> consumer = clientAPI -> {
                for (int i = 0; i < examples.length; i++) {
                    Example example = examples[i];

                    String tag = example.getClass().getSimpleName();
                    tag(tag, () -> example.run(clientAPI));

                    if (i != examples.length - 1) {
                        System.out.println();
                    }
                }
            };

            run(new TestRunnerContext().diskComponentCleanup(true).consumer(consumer));
        };
    }

    public static ExceptionRunnable configurableGroup(ConfigurableExample... examples) {
        if (examples == null || examples.length == 0) {
            return null;
        }

        return () -> {
            for (int i = 0; i < examples.length; i++) {
                ConfigurableExample example = examples[i];
                String tag = example.getClass().getSimpleName();

                TestRunnerContext context = new TestRunnerContext()
                        .consumer(clientAPI -> tag(tag, () -> example.run(clientAPI)))
                        .configurator(example.testConfigurator())
                        .diskComponentCleanup(true);

                run(context);

                if (i != examples.length - 1) {
                    System.out.println();
                }
            }
        };
    }

    public static ExceptionRunnable runnableGroup(ExceptionRunnable... examples) {
        if (examples == null || examples.length == 0) {
            return null;
        }

        return () -> {
            for (int i = 0; i < examples.length; i++) {
                examples[i].run();

                if (i != examples.length - 1) {
                    System.out.println();
                }
            }
        };
    }

    public static void separateLines(ExceptionRunnable... runnables) {
        if (runnables == null || runnables.length == 0) {
            return;
        }

        for (int i = 0; i < runnables.length; i++) {
            try {
                runnables[i].run();
            } catch (Exception e) {
                throw new RuntimeException(e);
            }

            if (i != runnables.length - 1) {
                System.out.println();
            }
        }
    }

    public static void tag(String tag, ExceptionRunnable runnable) throws Exception {
        startTag(tag);
        runnable.run();
        endTag(tag);
    }

    public static void startTag(String tag) {
        ConsoleColor.GREEN.print("<" + tag + ">");
    }

    public static void endTag(String tag) {
        ConsoleColor.GREEN.print("</" + tag + ">");
    }

    public static void deleteAllData() {
        deleteDirectory(new File(DIRECTORY_PREFIX));
    }

    private static String memberRoles(Map<Cluster.Role, List<GeneronNode>> nodeByRole) {
        List<GeneronNode> leaders = nodeByRole.get(Cluster.Role.LEADER);
        List<GeneronNode> candidates = nodeByRole.get(Cluster.Role.CANDIDATE);
        List<GeneronNode> followers = nodeByRole.get(Cluster.Role.FOLLOWER);

        String leaderPart = "Leader: ";

        if (leaders == null || leaders.size() != 1) {
            leaderPart += "none";
        } else {
            leaderPart += memberIds(leaders).get(0);
        }

        List<String> candidateIds = memberIds(candidates);
        List<String> followerIds = memberIds(followers);

        String candidatesPart = candidateIds == null ? null : "Candidates: " + String.join(", ", candidateIds);
        String followersPart = followerIds == null ? null : "Followers: " + String.join(", ", followerIds);

        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(leaderPart);

        if (candidatesPart != null) {
            stringBuilder.append(", ");
            stringBuilder.append(candidatesPart);
        }

        if (followersPart != null) {
            stringBuilder.append(", ");
            stringBuilder.append(followersPart);
        }

        return stringBuilder.toString();
    }

    private static List<String> memberIds(List<GeneronNode> candidates) {
        if (candidates == null || candidates.isEmpty()) {
            return null;
        }

        return candidates.stream()
                .map(GeneronNode::context)
                .map(GeneronNodeContext::clusterMember)
                .map(GeneronClusterMember::id)
                .sorted(Comparator.comparingInt(id -> id))
                .map(String::valueOf)
                .toList();
    }

    private static boolean deleteDirectory(File directory) {
        File[] allContents = directory.listFiles();

        if (allContents != null) {
            for (File file : allContents) {
                deleteDirectory(file);
            }
        }

        return directory.delete();
    }

}